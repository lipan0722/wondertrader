//
// Created by Administrator on 2022/3/20 0020.
//

#include <iomanip>
#include <regex>
#include <iostream>
#include <sstream>
#include <cmath>
#include "common.h"

#define CURL_STATICLIB

#include <curl/curl.h>

#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "../Share/fmtlib.h"

using namespace std;
using namespace rapidjson;

string get_trade_date() {
    /*获取交易日期*/
    string trade_date;
    time_t now = time(nullptr);   // 基于当前系统的当前日期/时间
    tm *ltm_now = localtime(&now);
    char buffer[20];
    if (ltm_now->tm_hour > 19) {
        //夜盘交易
        if (ltm_now->tm_wday == 5) {
            // 周五夜盘交易日期为下周一
            time_t next_monday = now + 3 * 60 * 60 * 24;
            tm *ltm_next_monday = localtime(&next_monday);
            strftime(buffer, sizeof(buffer), "%Y%m%d", ltm_next_monday);
            trade_date = string(buffer);
        } else {
            // 周一至周四夜盘交易日期为明日
            time_t tomorrow = now + 1 * 60 * 60 * 24;
            tm *ltm_tomorrow = localtime(&tomorrow);
            strftime(buffer, sizeof(buffer), "%Y%m%d", ltm_tomorrow);
            trade_date = string(buffer);
        }
    } else {
        strftime(buffer, sizeof(buffer), "%Y%m%d", ltm_now);
        trade_date = string(buffer);
    }
    return trade_date;
}

time_t StringToDatetime(const string &strDatetime) {
    tm tmDatetime{};                                    // 定义tm结构体。
    std::istringstream iss;
    iss.str(strDatetime);
    iss >> std::get_time(&tmDatetime, "%Y%m%d");
    return mktime(&tmDatetime);
}

short get_remaining_days(const string &trade_date, const string &symbol) {
    std::regex reg("\\d+");
    std::smatch m;
    std::regex_search(symbol, m, reg);
    string strNums = m[0];
    string dt{};
    if (strNums.length() == 4) {
        dt = "20" + strNums + "01";
    } else if (strNums.length() == 3) {
        dt = "202" + strNums + "01";
    } else {
        return -1;
    }
    time_t timeSymbolDate = StringToDatetime(dt);
    time_t timeTradeDate = StringToDatetime(trade_date);
    auto days = short(difftime(timeSymbolDate, timeTradeDate) / 3600 / 24);
    return days;
}

void initDatabase(const string &dbName) {
    try {
        // Open a database file in create/write mode
        SQLite::Database db(dbName, SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE);
        // position
        db.exec("create table if not exists position(\n"
                "    name                VARCHAR(50) not null primary key,\n"
                "    pos                 INTEGER      not null,\n"
                "    pos_long            INTEGER      not null,\n"
                "    pos_short           INTEGER      not null)");

        // trade
        db.exec("create table if not exists trade(\n"
                "    id        INTEGER      not null primary key AUTOINCREMENT,\n"
                "    name      VARCHAR(50) not null,\n"
                "    date      DATE         not null,\n"
                "    time      TIME         not null,\n"
                "    price     REAL         not null,\n"
                "    direction VARCHAR(2) not null,\n"
                "    offset    VARCHAR(2) not null,\n"
                "    volume    INTEGER      not null)");

        // tradebook
        db.exec("create table if not exists tradebook(\n"
                "    name   VARCHAR(50) not null,\n"
                "    price  REAL         not null,\n"
                "    buy    INTEGER      not null,\n"
                "    sell   INTEGER      not null,\n"
                "    bought INTEGER      not null,\n"
                "    sold   INTEGER      not null,\n"
                "    primary key (name, price))");

        // variable
        db.exec("create table if not exists variable(\n"
                "    key    VARCHAR(50) not null primary key,\n"
                "    value  VARCHAR (50) NOT NULL)");

    }
    catch (std::exception &e) {
        std::cout << "SQLite exception: " << e.what() << std::endl;
    }
}

/* 是否是合法的价格 */
bool price_valid(double price) {
    // 普通合约中0是非法价格，在价差合约中0是合理价格
    if (fabs(price) < 1e-6 || price > 999999 || price < 0) {
        return false;
    } else {
        return true;
    }
}

bool price_valid_sp(double price) {
    // 普通合约中0是非法价格，在价差合约中0是合理价格
    if (price < -999999 || price > 999999) {
        return false;
    } else {
        return true;
    }
}

/* 取整价格到合约最小价格变动 */
double round_to_pricetick(double price, double pricetick) {
    return round(price / pricetick) * pricetick;
}

size_t WritePostBodyResp(void *buffer, size_t size, size_t nmemb, void *userp) {
    ((string *) userp)->append((char *) buffer, 0, size * nmemb);
    return size * nmemb;
}

/* 获取tushare数据 */
string getTushareData(const string &_strJsonParam) {
    CURL *curl = nullptr;
    CURLcode res;
    string respBodyData;

    curl = curl_easy_init();
    if (curl == nullptr) {
        return "CURLE_FAILED_INIT";
    }
    struct curl_slist *headerlist = nullptr;

    // 设置表头，表头内容可能不同
    headerlist = curl_slist_append(headerlist, "Content-Type:application/x-www-form-urlencoded");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);

    // 设置URL
    curl_easy_setopt(curl, CURLOPT_URL, "http://api.tushare.pro");

    // 设置参数，比如"ParamName1=ParamName1Content&ParamName2=ParamName2Content&..."
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, _strJsonParam.c_str());

    // 设置为Post
    curl_easy_setopt(curl, CURLOPT_POST, 1);

    // 设置数据返回
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WritePostBodyResp);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &respBodyData);

    // 发送
    res = curl_easy_perform(curl);

    if (res != CURLE_OK) {
        // 获取详细错误信息
        const char *szErr = curl_easy_strerror(res);
        fprintf(stderr, "curl_easy_perform() failed: %s\n", szErr);
    }

    // 清空
    curl_easy_cleanup(curl);

    // 释放表头
    curl_slist_free_all(headerlist);

    return respBodyData;
}

/* 将普通期货合约代码转换为tushare合约代码 */
string get_ts_code_from_vt_symbol(const string &vt_symbol) {
    string::size_type pos = vt_symbol.find('.');
    string symbol = vt_symbol.substr(0, pos);
    string ts_code;
    if (vt_symbol.find("DCE") != string::npos) ts_code = vt_symbol;
    else if (vt_symbol.find("CZCE") != string::npos) {
        ts_code = symbol.substr(0, 2) + '2' + symbol.substr(2) + ".ZCE";
    } else if (vt_symbol.find("SHFE") != string::npos) {
        ts_code = symbol + ".SHF";
    }
    return ts_code;
}

/* 获取普通合约日线收盘价和交易量*/
map<string, pair<double, int>> get_fut_daily(const string &vt_symbol, const string &end_date) {
    map<string, pair<double, int>> map_date_price_volume{};
    string ts_code = get_ts_code_from_vt_symbol(vt_symbol);
    if (ts_code.length() < 1) return map_date_price_volume;
    string str_json_param = fmt::format("{{"
                                        "    \"api_name\":\"fut_daily\","
                                        "    \"token\":\"01403a310ad1878975ec569492b41c52e3b54106d0e5ad9097c8f4f4\","
                                        "    \"params\":{{"
                                        "        \"ts_code\":\"{}\","
                                        "        \"end_date\":\"{}\""
                                        "    }},"
                                        "    \"fields\":\"trade_date,close,vol\""
                                        "}}", ts_code, end_date);
    string result = getTushareData(str_json_param);
    // 解析结果
    /*
    {"request_id":"d7a44f90d9db11eca7ca056fb963fcf3",
     "code":0,
     "msg":"",
     "data":{"fields":["trade_date","close","vol"],"items":[["20220520",2995.0,15054.0]],
     "has_more":false}}
     */
    Document doc_result;
    doc_result.Parse(result.c_str());
    for (auto &item: doc_result["data"]["items"].GetArray()) {
        if (item[0].IsString() && item[1].IsNumber() && item[2].IsNumber()) {
            map_date_price_volume[item[0].GetString()] = pair<double, int>(item[1].GetDouble(), item[2].GetDouble());
        }
    }
    return map_date_price_volume;
}

// 获取配对合约日线收盘价的加权平均数
optional<double>
get_average_price_pair(const string &vt_symbol_init, const string &vt_symbol_passive, const string &end_date) {
    string ts_code_init = get_ts_code_from_vt_symbol(vt_symbol_init);
    string ts_code_passive = get_ts_code_from_vt_symbol(vt_symbol_passive);
    if (ts_code_init.length() < 1 || ts_code_passive.length() < 1) return nullopt;
    map<string, pair<double, int>> mapDatePriceVolumeInit{}, mapDatePriceVolumePassive{}, mapDatePriceVolumeSpread{};
    mapDatePriceVolumeInit = get_fut_daily(vt_symbol_init, end_date);
    mapDatePriceVolumePassive = get_fut_daily(vt_symbol_passive, end_date);

    // 计算配对合约价格和成交量
    for (const auto &[kInit, vInit]: mapDatePriceVolumeInit) {
        auto search = mapDatePriceVolumePassive.find(kInit);
        if (search != mapDatePriceVolumePassive.end()) {
            mapDatePriceVolumeSpread[kInit] = pair<double, int>(vInit.first - search->second.first,
                                                                min(vInit.second, search->second.second));
        }
    }
    double dSumPriceVolume{0}, dSumVolume{0};
    for (const auto &[k, v]: mapDatePriceVolumeSpread) {
        dSumPriceVolume += v.first * v.second;
        dSumVolume += v.second;
    }
    if (dSumVolume == 0) return nullopt;
    double dAveragePrice = dSumPriceVolume / dSumVolume;
    return dAveragePrice;
}

/* 获取单合约日线收盘价的加权平均数*/
optional<double> get_average_price(const string &vt_symbol, const string &end_date) {
    string ts_code = get_ts_code_from_vt_symbol(vt_symbol);
    if (ts_code.length() < 1) return nullopt;
    map<string, pair<double, int>> mapDatePriceVolume = get_fut_daily(vt_symbol, end_date);
    double dSumPriceVolume{0}, dSumVolume{0};
    for (const auto &[k, v]: mapDatePriceVolume) {
        dSumPriceVolume += v.first * v.second;
        dSumVolume += v.second;
    }
    if (dSumVolume == 0) return nullopt;
    double dAveragePrice = dSumPriceVolume / dSumVolume;
    return dAveragePrice;
}

/* 载入单个交易变量 */
string load_variable(SQLite::Database &db, const string &key) {
    string value;
    string sql = "SELECT count(*) FROM variable WHERE key = '" + key + "'";
    int count = db.execAndGet(sql);
    if (count >= 1) {
        sql = "SELECT value FROM variable WHERE key = '" + key + "'";
        value = db.execAndGet(sql).getString();
    } else {
        SQLite::Statement query(db, "INSERT INTO variable(key,value) VALUES (?, ?)");
        query.bind(1, key);
        query.bind(2, "");
        query.exec();
    }
    return value;
}

/* 保存单个交易变量 */
void save_variable(SQLite::Database &db, const string &key, const string &value) {
    SQLite::Statement query(db, "INSERT OR REPLACE INTO variable(key,value) VALUES (?, ?)");
    query.bind(1, key);
    query.bind(2, value);
    query.exec();
}

/* 更新仓位信息 */
void save_position(SQLite::Database &db, const Position &position) {
    string sql = "INSERT OR REPLACE INTO position(name,pos,pos_long,pos_short) VALUES (?, ?, ?, ?)";
    SQLite::Statement query(db, sql);
    query.bind(1, position.name);
    query.bind(2, position.pos);
    query.bind(3, position.pos_long);
    query.bind(6, position.pos_short);
    query.exec();
}

/* 输入要分割的字符串和分割的方式 */
vector<string> split(const string &str, const string &pattern) {
    vector<string> ret;
    if (pattern.empty()) return ret;
    size_t start = 0, index = str.find_first_of(pattern, 0);
    while (index != std::string::npos) {
        if (start != index)
            ret.push_back(str.substr(start, index - start));
        start = index + 1;
        index = str.find_first_of(pattern, start);
    }
    if (!str.substr(start).empty())
        ret.push_back(str.substr(start));
    return ret;
}


bool compare_trade_books(const map<double, TradeBook> &trade_books, const map<double, TradeBook> &trade_books_last) {
    if (trade_books.size() != trade_books_last.size()) return false;
    for (const auto &[k, v]: trade_books) {
        auto search = trade_books_last.find(k);
        if (search != trade_books_last.end()) {
            if (v.buy != search->second.buy || v.sell != search->second.sell
                || v.bought != search->second.bought || v.sold != search->second.sold) {
                return false;
            }
        } else {
            return false;
        }
    }
    return true;
}

/* 判断时间戳是否在区间中 */
bool check_time_in_range(time_t now, const string &begin, const string &end) {
    tm *tm_now = localtime(&now);
    tm tm_begin = *tm_now;
    tm tm_end = *tm_now;
    // 解析时间格式"15:11:12"
    vector<string> vec_begin = split(begin, ":");
    tm_begin.tm_hour = stoi(vec_begin[0]);
    tm_begin.tm_min = stoi(vec_begin[1]);
    tm_begin.tm_sec = stoi(vec_begin[2]);
    vector<string> vec_end = split(end, ":");
    tm_end.tm_hour = stoi(vec_end[0]);
    tm_end.tm_min = stoi(vec_end[1]);
    tm_end.tm_sec = stoi(vec_end[2]);

    time_t t_begin = mktime(&tm_begin);
    time_t t_end = mktime(&tm_end);
    if (now >= t_begin && now <= t_end) return true;
    else return false;
}

/* 判断时间戳是否在多个时间段内 */
/* "trade_time": ["09:00:05","14:59:55","21:00:05","22:59:55"] */
bool check_time_in_periods(const time_t &now, const vector<string> &time_periods) {
    if (time_periods.size() < 2 || time_periods.size() % 2 != 0)
        return false;
    for (size_t i = 0; i < time_periods.size(); i += 2) {
        if (check_time_in_range(now, time_periods[i], time_periods[i + 1])) return true;
    }
    return false;
}

/* 判断时间戳是否临近结束 */
bool check_time_near_end(time_t now, int seconds, const string &end) {
    tm *tm_now = localtime(&now);
    tm tm_end = *tm_now;
    // 解析时间格式"15:11:12"
    vector<string> vec_end = split(end, ":");
    tm_end.tm_hour = stoi(vec_end[0]);
    tm_end.tm_min = stoi(vec_end[1]);
    tm_end.tm_sec = stoi(vec_end[2]);
    time_t time_end = mktime(&tm_end);
    if (now >= time_end - seconds && now <= time_end) return true;
    else return false;
}

/* 比较订单登记簿，一致返回True,不一致返回False */
bool CompareTradeBooks(const map<double, TradeBook> &map1, const std::map<double, TradeBook> &map2) {
    if (map1.size() != map2.size()) {
        return false;
    }
    auto iter1 = map1.begin();
    auto iter2 = map2.begin();
    for (; iter1 != map1.end(); ++iter1, ++iter2) {
        if (iter1->first != iter2->first
            || iter1->second.buy != iter2->second.buy
            || iter1->second.sell != iter2->second.sell
            || iter1->second.bought != iter2->second.bought
            || iter1->second.sold != iter2->second.sold) {
            return false;
        }
    }
    return true;
}

/* 兼容wt和kungfu对于交易方向的判断，通过此函数进行转换
// wt对于isLong有特殊的处理,通过下表反向判断多空队列
// 多    开   ->  true
// 多    平   ->  false
// 空    开   ->  false
// 空    平   ->  true
 */
TThostFtdcDirectionType get_direction_by_islong_and_offset(bool isLong, uint32_t offset) {
    if (isLong) {
        if (offset == 0) return THOST_FTDC_D_Buy;
        else return THOST_FTDC_D_Sell;
    } else {
        if (offset == 0) return THOST_FTDC_D_Sell;
        else return THOST_FTDC_D_Buy;
    }
}

// 在价差合约代码中解析出主动合约代码和被动合约代码
// 价差合约[SPD RM709&RM801.CZCE]返回主动合约[RM709] 被动合约[RM801]
string parse_init_from_sp_symbol(const string &sp_symbol) {
    string::size_type begin = sp_symbol.find(' ') + 1;
    string::size_type end = sp_symbol.find('&');
    string::size_type len = end - begin;
    return sp_symbol.substr(begin, len);
}

string parse_passive_from_sp_symbol(const string &sp_symbol) {
    string::size_type begin = sp_symbol.find('&') + 1;
    string::size_type end = sp_symbol.find('.');
    string::size_type len = end - begin;
    return sp_symbol.substr(begin, len);
}

void update_position_by_trade(Position &position, bool isLong, bool isOpen, int volume) {
    if (isLong) {
        if (isOpen) position.pos_long += volume;
        else position.pos_short -= volume;
    } else {
        if (isOpen) position.pos_short += volume;
        else position.pos_long -= volume;
    }
    position.pos = position.pos_long - position.pos_short;
}