#include "WtUftStraNet.h"
#include "../Includes/IUftStraCtx.h"
#include "../Includes/WTSVariant.hpp"
#include "../Includes/WTSContractInfo.hpp"
#include "../Share/decimal.h"

extern const char *FACT_NAME;

WtUftStraNet::WtUftStraNet(const char *id) : UftStrategy(id), _channel_ready(false),
                                             db(string(id) + ".db", SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE) {
    strategy_name = id;
}

WtUftStraNet::~WtUftStraNet() = default;

const char *WtUftStraNet::getName() {
    return "WtUftStraNet";
}

const char *WtUftStraNet::getFactName() {
    return FACT_NAME;
}

bool WtUftStraNet::init(WTSVariant *cfg) {
    //这里演示一下外部传入参数的获取
    symbol = cfg->getCString("symbol");
    exchange = cfg->getCString("exchange");
    pricetick = cfg->getDouble("pricetick");
    step_size = cfg->getUInt32("step_size");
    deviation_adjust_buy = cfg->getInt32("deviation_adjust_buy");
    deviation_adjust_sell = cfg->getInt32("deviation_adjust_sell");
    weight = cfg->getUInt32("weight");
    zero_stop = cfg->getBoolean("zero_stop");
    exit_days = cfg->getUInt32("exit_days");
    average_adjust = cfg->getInt32("average_adjust");
    rebuild_book_step = cfg->getUInt32("rebuild_book_step");
    return true;
}

void WtUftStraNet::on_entrust(uint32_t localid, bool bSuccess, const char *message) {
    if (!bSuccess) {
        if (localid == orderid_buy) {
            orderid_buy = 0;
            _ctx->stra_log_info(fmt::format("on_entrust fail [{}],error[{}]:", localid, message).c_str());
        }
        if (localid == orderid_sell) {
            orderid_sell = 0;
            _ctx->stra_log_info(fmt::format("on_entrust fail [{}],error[{}]:", localid, message).c_str());
        }
    }
}

// 不依赖行情和交易服务连接的初始化
void WtUftStraNet::on_init(IUftStraCtx *ctx) {
    ctx->stra_log_info(fmt::format("on_init id[{}] name[{}]", ctx->id(), ctx->name()).c_str());
    _ctx = ctx;
    _ctx->stra_log_info(fmt::format("strategy [{}] init, params:", strategy_name).c_str());
    _ctx->stra_log_info(fmt::format("symbol:{},exchange:{},pricetick:{}",
                                    symbol, exchange, pricetick).c_str());
    _ctx->stra_log_info(fmt::format("step_size: {}", step_size).c_str());
    _ctx->stra_log_info(fmt::format("deviation_adjust_buy: {}", deviation_adjust_buy).c_str());
    _ctx->stra_log_info(fmt::format("deviation_adjust_sell: {}", deviation_adjust_sell).c_str());
    _ctx->stra_log_info(fmt::format("weight: {}", weight).c_str());
    _ctx->stra_log_info(fmt::format("zero_stop: {}", zero_stop).c_str());
    _ctx->stra_log_info(fmt::format("exit_days: {}", exit_days).c_str());
    _ctx->stra_log_info(fmt::format("rebuild_book_step: {}", rebuild_book_step).c_str());
    _ctx->stra_log_info(fmt::format("average_adjust: {}", average_adjust).c_str());

    // 判断交易的是套利合约还是普通合约
    if (symbol.find('&') == string::npos) is_spread_mode = false;

    // 交易变量
    vt_symbol = symbol + "." + exchange;
    wt_symbol = exchange + "." + symbol;
    trade_date = get_trade_date();
    optional<double> average_price_sp_old;
    if (is_spread_mode) {
        // 解析主动合约和被动合
        symbol_init = parse_init_from_sp_symbol(vt_symbol);
        symbol_passive = parse_passive_from_sp_symbol(vt_symbol);
        if (symbol_init.empty() || symbol_passive.empty()) {
            _ctx->stra_log_info("Parse init and passive error, check spread code.");
        } else {
            _ctx->stra_log_info(fmt::format("symbol_init[{}] symbol_passive[{}]", symbol_init, symbol_passive).c_str());
        }
        vt_symbol_init = symbol_init + "." + exchange;
        vt_symbol_passive = symbol_passive + "." + exchange;
        wt_symbol_init = exchange + "." + symbol_init;
        wt_symbol_passive = exchange + "." + symbol_passive;
        // 合约剩余天数
        remaining_days = min(get_remaining_days(trade_date, symbol_init),
                             get_remaining_days(trade_date, symbol_passive));
        average_price_sp_old = get_average_price_pair(vt_symbol_init, vt_symbol_passive, trade_date);

    } else {
        remaining_days = get_remaining_days(trade_date, symbol);
        average_price_sp_old = get_average_price(vt_symbol, trade_date);

    }
    _ctx->stra_log_info(fmt::format("trade_date:{} exit_days:{} remaining_days:{}",
                                    trade_date, exit_days, remaining_days).c_str());
    // 取交易时间段
    WTSCommodityInfo *_comm_info = ctx->stra_get_comminfo(is_spread_mode ? wt_symbol_init.c_str() : wt_symbol.c_str());
    _session_info = _comm_info->getSessionInfo();
    _ctx->stra_log_info(fmt::format("Session begin:{} end:{}",
                                    _session_info->getOpenTime(), _session_info->getCloseTime()).c_str());
    // 计算合约均价
    if (average_price_sp_old == nullopt) {
        _ctx->stra_log_info("Calculate average price fail, terminate.");
        exit(1);
    }
    sp_average_price = round_to_pricetick(average_price_sp_old.value() + float(average_adjust) * pricetick,
                                          pricetick);
    _ctx->stra_log_info(fmt::format("Original average price: {} adjust: {} new: {}",
                                    average_price_sp_old.value(), average_adjust,
                                    sp_average_price.value()).c_str());
    // order
    orderid_buy = 0;
    orderid_sell = 0;

    _ctx->stra_log_info("----------load database data begin----------");
    // 初始化数据库,建表
    initDatabase(strategy_name + ".db");

    // 获取当前委托登记薄
    load_trade_books();
    print_trade_books();
    // 重新构建tradebooks
    if (rebuild_book_step > 0) rebuild_tradebook();
    _ctx->stra_log_info("----------load database data end----------");
    isStarted = true;
}

//更新仓位
void WtUftStraNet::update_position() {
    _ctx->stra_log_info(fmt::format("update_position:{},{}", strategy_name, wt_symbol).c_str());
    double _pos = _ctx->stra_enum_position(wt_symbol.c_str());
    if (decimal::eq(_pos, 0)) {
        _ctx->stra_log_info(fmt::format("{} position empty，position init end.", wt_symbol).c_str());
        position_inited = true;
    }
}

void WtUftStraNet::load_trade_books() {
    trade_books.clear();
    SQLite::Statement query(db, "SELECT * FROM tradebook");
    while (query.executeStep()) {
        TradeBook book = {
                query.getColumn(0),
                query.getColumn(1),
                0,
                0,
                query.getColumn(4),
                query.getColumn(5)};
        trade_books.insert(pair<double, TradeBook>(query.getColumn(1), book));
    }
}


void WtUftStraNet::print_trade_books() {
    if (CompareTradeBooks(trade_books, trade_books_last)) return;
    _ctx->stra_log_info(fmt::format("----- books & pos {} {} & avg {} -----",
                                    position.pos_long, position.pos_short, sp_average_price.value()).c_str());
    for (auto rit = trade_books.rbegin(); rit != trade_books.rend(); rit++) {
        const TradeBook &b = rit->second;
        if (b.buy != 0 || b.sell != 0 || b.bought != 0 || b.sold != 0) {
            _ctx->stra_log_info(fmt::format("{:<6}{:>5}{:>6}{:>6}{:>6}",
                                            b.price, b.buy, b.sell, b.bought, b.sold).c_str());
        }
    }
    trade_books_last = trade_books;
}

void WtUftStraNet::rebuild_tradebook() {
    if (trade_books.empty()) return;
    // process bought
    optional<double> max_bought = get_max_bought();
    if (max_bought != nullopt && (max_bought > sp_average_price || remaining_days < exit_days)) {
        // 从小到大处理
        for (auto &[k, v]: trade_books) {
            if (v.bought < 1) continue;
            else {
                v.bought -= 1;
                TradeBook &new_bought_book = get_trade_book(k - pricetick * float(rebuild_book_step));
                new_bought_book.bought += 1;
            }
        }
    }
    // process sold
    optional<double> min_sold = get_min_sold();
    if (min_sold != nullopt && (min_sold < sp_average_price || remaining_days < exit_days)) {
        // 从大到小处理
        for (auto rit = trade_books.rbegin(); rit != trade_books.rend(); rit++) {
            TradeBook &b = rit->second;
            if (b.sold < 1) continue;
            else {
                b.sold -= 1;
                TradeBook &new_sold_book = get_trade_book(b.price + pricetick * float(rebuild_book_step));
                new_sold_book.sold += 1;
            }
        }
    }
    _ctx->stra_log_info("----------after rebuild, new trade books----------");
    print_trade_books();
}

// 取book中最大买入价，如果没有返回nullopt
optional<double> WtUftStraNet::get_max_bought() {
    optional<double> p;
    for (const auto &[_, b]: trade_books) {
        if (b.bought >= 1 && (p == nullopt || b.price > p)) p = b.price;
    }
    return p;
}

// 取book中最小买入价，如果没有返回nullopt
optional<double> WtUftStraNet::get_min_bought() {
    optional<double> p;
    for (const auto &[_, b]: trade_books) {
        if (b.bought >= 1 && (p == nullopt || b.price < p)) p = b.price;
    }
    return p;
}

// 取book中最小卖出价，如果没有返回nullopt
optional<double> WtUftStraNet::get_min_sold() {
    optional<double> p;
    for (const auto &[_, b]: trade_books) {
        if (b.sold >= 1 && (p == nullopt || b.price < p)) p = b.price;
    }
    return p;
}

// 取book中最大卖出价，如果没有返回nullopt
optional<double> WtUftStraNet::get_max_sold() {
    optional<double> p;
    for (const auto &[_, b]: trade_books) {
        if (b.sold >= 1 && (p == nullopt || b.price > p)) p = b.price;
    }
    return p;
}

/*
	判断价格是否存在交易登记簿中，
	有则直接返回，无则新建空项目返回
 */
TradeBook &WtUftStraNet::get_trade_book(double price) {
    double new_price = round_to_pricetick(price, pricetick);
    auto iter = trade_books.find(new_price);
    if (iter != trade_books.end()) {
        return iter->second;
    } else {
        trade_books[new_price] = TradeBook{strategy_name, new_price, 0, 0, 0, 0};
        return trade_books[new_price];
    }
}


/* 打印tick数据 */
void WtUftStraNet::print_tick() {
    if (price_buy != price_buy_last || price_sell != price_sell_last) {
        _ctx->stra_log_info(fmt::format("tick buy={} {:3} ({}), sell={} {:3} ({})",
                                        price_buy.value(), volume_buy.value(), orderid_buy,
                                        price_sell.value(), volume_sell.value(), orderid_sell).c_str());
        price_buy_last = price_buy;
        price_sell_last = price_sell;
    }
}


void WtUftStraNet::on_tick(IUftStraCtx *ctx, const char *code, WTSTickData *newTick) {
    // ctx->stra_log_info(fmt::format("on_tick {} buy:{} sell:{}", code, newTick->bidprice(0), newTick->askprice(0)).c_str());
    if (!_channel_ready || !isStarted) return;
    //初始化仓位
    if (!position_inited) update_position();
    // 过滤非本策略回报
    if (code != wt_symbol) {
        _ctx->stra_log_error(fmt::format("other symbol: {}", code).c_str());
        return;
    }
    // 策略停止判断
    if (zero_stop && position.pos_long == 0 && position.pos_short == 0) {
        _ctx->stra_cancel(orderid_buy);
        _ctx->stra_cancel(orderid_sell);
        _ctx->stra_log_info("Zero_stop is true and position is empty, stop running!!!");
        return;
    }
    if (remaining_days < exit_days && position.pos_long == 0 && position.pos_short == 0) {
        _ctx->stra_cancel(orderid_buy);
        _ctx->stra_cancel(orderid_sell);
        _ctx->stra_log_info("The contract is about to expire，stop running!!!");
        return;
    }

    // 挂单量为0时表示没人挂单，此时不能按价格0下单
    // 这里price_valid_sp比price_valid条件更宽松，统一用这个判断
    if (newTick->bidqty(0) == 0 || !price_valid_sp(newTick->bidprice(0))) {
        price_buy = nullopt;
    } else {
        price_buy = round_to_pricetick(newTick->bidprice(0), pricetick);
        volume_buy = newTick->bidqty(0);
    }
    if (newTick->askqty(0) == 0 || !price_valid_sp(newTick->askprice(0))) {
        price_sell = nullopt;
    } else {
        price_sell = round_to_pricetick(newTick->askprice(0), pricetick);
        volume_sell = newTick->askqty(0);
    }
    if (!price_limit_up.has_value() || !price_limit_down.has_value()) {
        price_limit_up = newTick->upperlimit();
        price_limit_down = newTick->lowerlimit();
        _ctx->stra_log_info(fmt::format("Limit down price [{}],Limit up price [{}]",
                                        price_limit_down.value(), price_limit_up.value()).c_str());
    }
    if (!(price_buy.has_value() && price_sell.has_value())) {
        return;
    }
    print_tick();
    // 交易
    trade_depend_tick();
    print_trade_books();
}


void WtUftStraNet::trade_depend_tick() {
    //  ---------- 卖出逻辑 ----------
    // 对价卖出，避免初次运行时不利开仓
    if (price_buy.has_value() && (position.pos_long > 0 || position.pos_short > 0))
        prepare_sell_order(price_buy.value());
    // 插队卖出
    if (price_sell.has_value() && price_buy.has_value()) {
        double jump_price_sell = round_to_pricetick(price_sell.value() - pricetick, pricetick);
        if (jump_price_sell > price_buy) {
            uint32_t jump_sell_volume = cal_sell_volume(jump_price_sell);
            if (jump_sell_volume > 0)
                prepare_sell_order(jump_price_sell);
        }
    }
    // 排队卖出
    if (price_sell.has_value()) prepare_sell_order(price_sell.value());

    // ---------- 买入逻辑 ----------
    // 对价买入，避免初次运行时不利开仓
    if (price_sell.has_value() && (position.pos_long > 0 || position.pos_short > 0))
        prepare_buy_order(price_sell.value());
    // 插队买入
    if (price_buy.has_value() && price_sell.has_value()) {
        double jump_price_buy = round_to_pricetick(price_buy.value() + pricetick, pricetick);
        if (jump_price_buy < price_sell) {
            uint32_t jump_buy_volume = cal_buy_volume(jump_price_buy);
            if (jump_buy_volume > 0)
                prepare_buy_order(jump_price_buy);
        }
    }
    // 排队买入
    if (price_buy.has_value()) prepare_buy_order(price_buy.value());
}


void WtUftStraNet::prepare_sell_order(double sell_price) {
    uint32_t sell_volume = cal_sell_volume(sell_price);
    if (sell_volume >= 1) {
        if (orderid_sell == 0) {
            orderid_sell = _ctx->stra_sell(wt_symbol.c_str(), sell_price, sell_volume, 0);
            _ctx->stra_log_info(fmt::format("prepare_sell_order {} {} {}",
                                            orderid_sell, sell_price, sell_volume).c_str());
        } else if (auto search = orders.find(orderid_sell); search != orders.end()) {
            const auto &_order = search->second;
            if (!_order.isCanceled && _order.leftQty > 0) {
                double order_sell_price = _order.price;
                if (sell_price < order_sell_price) {
                    // 这里下单后撤单太快可能导致撤单失败需要后续处理
                    // 先开后撤是为了避免撤单响应太慢导致平仓失败
                    _ctx->stra_log_info(fmt::format("Change Sell:cancel[{}][{}]->{},",
                                                    orderid_sell, order_sell_price, sell_price).c_str());
                    uint32_t _tmp_id = orderid_sell;
                    orderid_sell = _ctx->stra_sell(wt_symbol.c_str(), sell_price, sell_volume, 0);
                    _ctx->stra_log_info(fmt::format("prepare_sell_order {} {} {}",
                                                    orderid_sell, sell_price, sell_volume).c_str());
                    _ctx->stra_cancel(_tmp_id);


                }
            }
        }
    }
}


void WtUftStraNet::prepare_buy_order(double buy_price) {
    uint32_t buy_volume = cal_buy_volume(buy_price);
    if (buy_volume >= 1) {
        if (orderid_buy == 0) {
            orderid_buy = _ctx->stra_buy(wt_symbol.c_str(), buy_price, buy_volume, 0);
            _ctx->stra_log_info(
                    fmt::format("prepare_buy_order {} {} {}", orderid_buy, buy_price, buy_volume).c_str());
        } else if (auto search = orders.find(orderid_buy); search != orders.end()) {
            const auto &_order = search->second;
            if (!_order.isCanceled && _order.leftQty > 0) {
                double order_buy_price = _order.price;
                if (buy_price > order_buy_price) {
                    // 这里下单后撤单太快可能导致撤单失败需要后续处理
                    // 先开后撤是为了避免撤单响应太慢导致平仓失败
                    uint32_t _tmp_id = orderid_buy;
                    _ctx->stra_log_info(fmt::format("Change Buy:cancel[{}][{}] -> {},",
                                                    orderid_buy, order_buy_price, buy_price).c_str());
                    orderid_buy = _ctx->stra_buy(wt_symbol.c_str(), buy_price, buy_volume, 0);
                    _ctx->stra_log_info(fmt::format("prepare_buy_order {} {} {}",
                                                    orderid_buy, buy_price, buy_volume).c_str());
                    _ctx->stra_cancel(_tmp_id);
                }
            }
        }
    }
}


uint32_t WtUftStraNet::cal_sell_volume(double _price_sell) {
    uint32_t v = 0;
    // 无对冲时，上涨阶段可卖出开仓，只能以登记簿最高价卖出
    if (_price_sell >= sp_average_price && is_highest_sold_of_book(_price_sell)) {
        v = weight;
        v += int((_price_sell - sp_average_price.value()) / (deviation_adjust_sell * pricetick));
    } else {
        // 下跌阶段只能对冲价低买入
        for (const auto &[_, b]: trade_books) {
            if (b.bought < 1 || b.price > _price_sell - double(step_size) * pricetick) {
                continue;
            } else {
                v += b.bought;
            }
        }
    }
    // 减去已卖
    uint32_t _sum_sold = sum_sold(_price_sell);
    if (v > _sum_sold) return v - _sum_sold;
    else return 0;
}

uint32_t WtUftStraNet::sum_sold(double price) {
    uint32_t sold = 0;
    for (const auto &[_, b]: trade_books) {
        if (b.price > (price - double(step_size) * pricetick) &&
            b.price < price + double(step_size) * pricetick)
            sold += b.sold;
    }
    return sold;
}

bool WtUftStraNet::is_highest_sold_of_book(double price) {
    function<bool(pair<double, TradeBook>)> is_highest_sold = [price](const pair<double, TradeBook> &p) -> bool {
        return !(p.second.sold > 0 && p.second.price > price);
    };
    return all_of(trade_books.begin(), trade_books.end(), is_highest_sold);
}


uint32_t WtUftStraNet::cal_buy_volume(double buy_price) {
    uint32_t v = 0;
    // 下跌阶段可买入开仓，只能以登记簿最低价买入
    if (buy_price <= sp_average_price && is_lowest_bought_of_book(buy_price)) {
        v = weight;
        v += int((sp_average_price.value() - buy_price) / (deviation_adjust_buy * pricetick));
    } else {
        // 上涨阶段只能对冲价高卖出
        for (const auto &[_, b]: trade_books) {
            if (b.sold >= 1 && b.price >= buy_price + double(step_size) * pricetick) {
                v += b.sold;
            }
        }
    }
    // 减去已买
    uint32_t _sum_bought = sum_bought(buy_price);
    if (v > _sum_bought) return v - _sum_bought;
    else return 0;
}

bool WtUftStraNet::is_lowest_bought_of_book(double price) {
    function<bool(pair<double, TradeBook>)> is_lowest_bought = [price](
            const pair<double, TradeBook> &p) -> bool {
        return !(p.second.bought > 0 && p.second.price < price);
    };
    return all_of(trade_books.begin(), trade_books.end(), is_lowest_bought);
}

uint32_t WtUftStraNet::sum_bought(double price) {
    uint32_t bought = 0;
    for (const auto &[_, b]: trade_books) {
        if (b.price > (price - double(step_size) * pricetick) &&
            b.price < (price + double(step_size) * pricetick))
            bought += b.bought;
    }
    return bought;
}


void WtUftStraNet::on_bar(IUftStraCtx *ctx, const char *code,
                          const char *period, uint32_t times, WTSBarStruct *newBar) {
    _ctx->stra_log_info("on_bar...");
}

void WtUftStraNet::trade_depend_book() {
    uint32_t _curTime = _ctx->stra_get_time();
    bool _isInTradingTime = _session_info->isInTradingTime(_curTime);
    if (_channel_ready && isStarted && _isInTradingTime) {
        optional<double> _max_sold = get_max_sold();
        optional<double> _min_bought = get_min_bought();
        // buy
        if (_max_sold.has_value()) {
            _ctx->stra_log_info("trade_depend_book buy - close max sold.");
            double _buy_price = round_to_pricetick(_max_sold.value() - pricetick * step_size, pricetick);
            prepare_buy_order(_buy_price);
        }
        if (_min_bought.has_value()) {
            _ctx->stra_log_info("trade_depend_book buy - open min bought.");
            double _buy_price = round_to_pricetick(_min_bought.value() - pricetick * step_size, pricetick);
            prepare_buy_order(_buy_price);
        }
        // sell
        if (_min_bought.has_value()) {
            _ctx->stra_log_info("trade_depend_book sell - close min bought.");
            double _sell_price = round_to_pricetick(_min_bought.value() + pricetick * step_size, pricetick);
            prepare_sell_order(_sell_price);
        }
        if (_max_sold.has_value()) {
            _ctx->stra_log_info("trade_depend_book sell - open max sold.");
            double _sell_price = round_to_pricetick(_max_sold.value() + pricetick * step_size, pricetick);
            prepare_sell_order(_sell_price);
        }
    }
}

void WtUftStraNet::on_minute_end(IUftStraCtx *ctx, uint32_t curTime) {
    if (curTime == 1015 || curTime == 1130 || curTime == 1500 || curTime == 2300) {
        _ctx->stra_log_info(fmt::format("on_minute_end [{}] now dump data ...", curTime).c_str());
        dump_trade_book();
        dump_position();
        dump_trade_all();
    }
    // 在交易时间段内、行情不活跃时主动下单
    trade_depend_book();
    // 改价时可能会由于下单后撤单太快可能导致撤单失败，需要定时统一处理
    for (const auto &[_id, _order]: orders) {
        if (_id != orderid_buy && _id != orderid_sell && _order.leftQty > 0 && !_order.isCanceled) {
            _ctx->stra_log_info(fmt::format("Cancel uncontrolled order:", _id).c_str());
            _ctx->stra_cancel(_id);
        }
    }
}

void WtUftStraNet::on_trade(IUftStraCtx *ctx, uint32_t localid, const char *stdCode, bool isLong,
                            uint32_t offset, double qty, double price) {}


void WtUftStraNet::on_position(IUftStraCtx *ctx, const char *stdCode, bool isLong, double prevol, double preavail,
                               double newvol, double newavail) {
    if (stdCode != wt_symbol) {
        // _ctx->stra_log_error(fmt::format("on_position wrong symbol: {}", stdCode).c_str());
        return;
    }
    if (isLong) position.pos_long = int(newvol + prevol);
    else position.pos_short = int(newvol + prevol);
    position.pos = position.pos_long - position.pos_short;
    position_inited = true;
    ctx->stra_log_info(fmt::format("[on_position] {}\t{}", position.pos_long, position.pos_short).c_str());
}

void WtUftStraNet::on_order(IUftStraCtx *ctx, uint32_t localid, const char *stdCode, bool isLong, uint32_t offset,
                            double totalQty, double leftQty, double price, bool isCanceled) {
    // _ctx->stra_log_info(fmt::format("on_order id[{}] name[{}]", ctx->id(), ctx->name()).c_str());
    // 过滤非本策略回报
    if (stdCode != wt_symbol) {
        // _ctx->stra_log_error(fmt::format("other symbol: {}", code).c_str());
        return;
    }
    auto _totalQty = uint32_t(totalQty);
    auto _leftQty = uint32_t(leftQty);
    bool _isOpen = (offset == 0);
    uint32_t trade_volume;
    if (auto search = orders.find(localid); search == orders.end()) {
        // 下单成功时会新增订单
        ctx->stra_log_info(fmt::format("on_order new {} leftQty:{} isCanceled:{}",
                                       localid, leftQty, isCanceled).c_str());
        orders[localid] = OrderData{stdCode, price, isLong, offset, _totalQty, _leftQty, isCanceled};
        TradeBook &_book = get_trade_book(price);
        if (isLong) _book.buy += _totalQty;
        else _book.sell += _totalQty;
        print_trade_books();
        trade_volume = _totalQty - _leftQty;
    } else {
        // 更新订单信息
        ctx->stra_log_info(fmt::format("on_order update {} leftQty:{} isCanceled:{}",
                                       localid, leftQty, isCanceled).c_str());
        trade_volume = orders[localid].leftQty - _leftQty;
        orders[localid].leftQty = _leftQty;
        orders[localid].isCanceled = isCanceled;
    }

    // 更新订单薄
    TradeBook &trade_book_item = get_trade_book(price);
    if (trade_volume > 0) {
        if (isLong) {
            // 更新委托
            trade_book_item.buy -= trade_volume;
            // 更新成交 - 开仓
            if (_isOpen) trade_book_item.bought += trade_volume;
            else {
                // 更新成交 - 对冲已卖 - 从高到低对冲
                uint32_t buy_volume = trade_volume;
                for (auto riter = trade_books.rbegin(); riter != trade_books.rend(); riter++) {
                    TradeBook &b = riter->second;
                    if (b.sold >= 1) {
                        uint32_t deal_volume = min(b.sold, buy_volume);
                        b.sold -= deal_volume;
                        buy_volume -= deal_volume;
                        if (buy_volume <= 0) break;
                    }
                }
            }
        } else {
            // 更新委托
            trade_book_item.sell -= trade_volume;
            // 更新成交 - 开仓
            if (_isOpen) trade_book_item.sold += trade_volume;
            else {
                // 更新成交 - 对冲已买 - 从低到高对冲
                uint32_t sell_volume = trade_volume;
                for (auto &trade_book: trade_books) {
                    TradeBook &b = trade_book.second;
                    if (b.bought >= 1) {
                        uint32_t deal_volume = min(b.bought, sell_volume);
                        b.bought -= deal_volume;
                        sell_volume -= deal_volume;
                        if (sell_volume <= 0) break;
                    }
                }
            }
        }
        // 记录成交
        _ctx->stra_log_info(fmt::format("sp_trade {} {} {} {} {}", localid, price,
                                        isLong ? "Buy" : "Sell", decodeOffset(offset), trade_volume).c_str());
        string _now = TimeUtils::getLocalTime();
        TradeData _trade{_now, strategy_name, price, isLong, offset, trade_volume};
        trade_all.push_back(_trade);
        // 更新仓位
        update_position_by_trade(position, isLong, _isOpen, int(trade_volume));
        print_trade_books();
    }
    // 已撤销、拒单
    if (isCanceled) {
        if (isLong) trade_book_item.buy -= _leftQty;
        else trade_book_item.sell -= _leftQty;
        print_trade_books();
    }
    // 不成功、撤销和全部成交时更新委托单
    if (isCanceled || _leftQty == 0) {
        if (orderid_buy == localid) orderid_buy = 0;
        if (orderid_sell == localid) orderid_sell = 0;
    }
    // 再次委托
    trade_depend_book();
}

// wt中仓位由底层管理，策略只跟踪仓位信息
void WtUftStraNet::print_position() {
    _ctx->stra_log_info(fmt::format("position {}\t{}", position.pos_long, position.pos_short).c_str());
}

// 依赖行情和交易服务连接的初始化
void WtUftStraNet::on_channel_ready(IUftStraCtx *ctx) {
    _ctx->stra_log_info(fmt::format("on_channel_ready id[{}] name[{}]", ctx->id(), ctx->name()).c_str());
    //初始化仓位
    position.name = strategy_name;
    update_position();
    print_position();

    // 是否有未完订单
    double undone = _ctx->stra_get_undone(wt_symbol.c_str());
    _ctx->stra_log_info(fmt::format("{} has not under control orders:{}", wt_symbol, undone).c_str());

    // 考虑日盘接续夜盘时需要重新订阅行情，将订阅移到此处
    _ctx->stra_sub_ticks(wt_symbol.c_str());
    _ctx->stra_sub_order_queues(wt_symbol.c_str());
    _ctx->stra_sub_order_details(wt_symbol.c_str());
    _ctx->stra_log_info("Subscribe market successfully.");

    _channel_ready = true;
}

void WtUftStraNet::on_channel_lost(IUftStraCtx *ctx) {
    _ctx->stra_log_info("on_channel_lost ......");
    _channel_ready = false;
    dump_trade_book();
    dump_position();
    dump_trade_all();
}


void WtUftStraNet::dump_trade_book() {
    _ctx->stra_log_info(fmt::format("dump_trade_book, trade_books.size={}", trade_books.size()).c_str());
    try {
        db.exec("DELETE FROM tradebook");
        int insert_nums = 0;
        for (const auto &[_, b]: trade_books) {
            if (b.buy == 0 && b.sell == 0 && b.bought == 0 && b.sold == 0) continue;
            SQLite::Statement insert(db, "insert into tradebook(name,price,buy,sell,bought,sold) "
                                         "VALUES (?, ?,?, ?,?, ?)");
            insert.bind(1, b.name);
            insert.bind(2, b.price);
            insert.bind(3, b.buy);
            insert.bind(4, b.sell);
            insert.bind(5, b.bought);
            insert.bind(6, b.sold);
            insert.exec();
            insert_nums++;
        }
    }
    catch (std::exception &e) {
        _ctx->stra_log_error(fmt::format("SQLite exception: {}", e.what()).c_str());
    }
}

void WtUftStraNet::dump_trade_all() {
    _ctx->stra_log_info("dump_trade_all ...");
    try {
        for (auto it = trade_all.begin(); it != trade_all.end();) {
            SQLite::Statement insert(db, "insert into trade(name,date, time, price, direction, offset, volume) "
                                         "VALUES (?, ?, ?, ?, ?, ?, ?)");
            insert.bind(1, it->stdCode);
            insert.bind(2, trade_date);
            insert.bind(3, it->time);
            insert.bind(4, it->price);
            insert.bind(5, it->isLong ? "Buy " : "Sell");
            insert.bind(6, decodeOffset(it->offset));
            insert.bind(7, it->qty);
            insert.exec();
            it = trade_all.erase(it);
        }
    }
    catch (std::exception &e) {
        _ctx->stra_log_error(fmt::format("SQLite exception: {}", e.what()).c_str());
    }
}

void WtUftStraNet::dump_position() {
    _ctx->stra_log_info("dump_position ...");
    try {
        db.exec("DELETE FROM position");
        SQLite::Statement insert(db, "insert into position(name,pos,pos_long,pos_short) VALUES (?, ? , ?, ?)");
        insert.bind(1, strategy_name);
        insert.bind(2, position.pos);
        insert.bind(3, position.pos_long);
        insert.bind(4, position.pos_short);
        insert.exec();
    }
    catch (std::exception &e) {
        _ctx->stra_log_error(fmt::format("SQLite exception: {}", e.what()).c_str());
    }
}

void WtUftStraNet::on_exit(IUftStraCtx *ctx) {
    ctx->stra_log_info("on_exit cancel orders and dump data ...");
    isStarted = false;
    _ctx->stra_log_info(fmt::format("stra_cancel_all: {}", wt_symbol).c_str());
    ctx->stra_cancel_all(wt_symbol.c_str());
    dump_trade_book();
    dump_trade_all();
    dump_position();
}
