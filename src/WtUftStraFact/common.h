//
// Created by Administrator on 2022/3/20 0020.
//

#ifndef STRATEGY_COMMON_H
#define STRATEGY_COMMON_H

#include <optional>
#include <string>
#include <ctime>
#include <deque>
#include <cmath>
#include <SQLiteCpp/SQLiteCpp.h>

#include "../Includes/UftStrategyDefs.h"
#include "../API/CTP6.3.15/ThostFtdcUserApiStruct.h"
#include "../Includes/ExecuteDefs.h"

using namespace std;


struct Position {
    string name;
    int pos{0};
    int pos_long{0};
    int pos_short{0};
};

struct TradeBook {
    string name;
    double price;
    uint32_t buy;
    uint32_t sell;
    uint32_t bought;
    uint32_t sold;
};


struct OrderData {
    const char *stdCode{};
    double price{};
    bool isLong{};
    uint32_t offset{0};
    uint32_t totalQty{0};
    uint32_t leftQty{0};
    bool isCanceled{false};
};

struct TradeData {
    string time;            //成交时间
    string stdCode;         //合约代码
    double price;           //价格
    bool isLong;            //买卖方向
    uint32_t offset;        //开平标志
    uint32_t qty;           //数量
};

/* 比较订单登记簿，一致返回True,不一致返回False */
bool CompareTradeBooks(const map<double, TradeBook> &map1, const std::map<double, TradeBook> &map2);

string get_trade_date();

// 获取当前交易日期距离最后交易日的天数
// get_remaining_days('20210501', 'm2105') -> 0
// get_remaining_days('20200401', 'SR101') -> 275
// get_remaining_days('20201215', 'SR101') -> 17
// get_remaining_days('20200401', 'RM009') -> 153
short get_remaining_days(const string &trade_date, const string &symbol);

// 初始化sqlite数据库,表不存在则建表
void initDatabase(const string &dbName);

/* 是否是合法的价格 */
bool price_valid(double price);

bool price_valid_sp(double price);

/* 取整价格到合约最小价格变动 */
double round_to_pricetick(double price, double pricetick);

/* 获取tushare数据 */
string getTushareData(const string &_strJsonParam);

/*
 将普通期货合约代码转换为tushare合约代码
 交易所名称	        交易所代码	合约后缀
 郑州商品交易所	    CZCE	    .ZCE
 上海期货交易所	    SHFE	    .SHF
 大连商品交易所	    DCE	        .DCE
 中国金融期货交易所	CFFEX	    .CFX
 上海国际能源交易所	INE	        .INE
 RM101.CZCE    ->     RM2101.ZCE
 TA012.CZCE    ->     TA2012.ZCE
 c2101.DCE     ->     c2101.DCE
 m2107.DCE     ->     m2107.DCE
 al2105.SHFE   ->     al2105.SHF
 */
string get_ts_code_from_vt_symbol(const string &vt_symbol);

/* 获取普通合约日线收盘价的加权平均数*/
optional<double> get_average_price(const string &vt_symbol, const string &end_date);

/* 获取配对合约日线收盘价的加权平均数*/
optional<double>
get_average_price_pair(const string &vt_symbol_init, const string &vt_symbol_passive, const string &end_date);

/* 解码交易方向*/
inline const char *decodeDirection(TThostFtdcDirectionType direction) {
    if (direction == THOST_FTDC_D_Buy) return "Buy";
    else return "Sell";
}

/* 解码交易开平  offset */
inline const char *decodeOffset(uint32_t offset) {
    switch (offset) {
        case 0:
            return "Open";
        case 1:
            return "Close";
        case 2:
            return "CloseToday";
        default:
            return "Unknown";
    }
}

/* 解码订单状态  LfOrderStatusType */
inline const char *decodeOrderStatus(char orderStatusType) {
    switch (orderStatusType) {
        case THOST_FTDC_OST_AllTraded:
            return "AllTraded";
        case THOST_FTDC_OST_PartTradedQueueing:
            return "PartTradedQueueing";
        case THOST_FTDC_OST_PartTradedNotQueueing:
            return "PartTradedNotQueueing";
        case THOST_FTDC_OST_NoTradeQueueing:
            return "NoTradeQueueing";
        case THOST_FTDC_OST_NoTradeNotQueueing:
            return "NoTradeNotQueueing";
        case THOST_FTDC_OST_Canceled:
            return "Canceled";
        case THOST_FTDC_OST_Unknown:
            return "Unknown";
        case THOST_FTDC_OST_NotTouched:
            return "NotTouched";
        case THOST_FTDC_OST_Touched:
            return "Touched";
        default:
            return reinterpret_cast<const char *>(orderStatusType);
    }
}

/* 载入单个交易变量 */
string load_variable(SQLite::Database &db, const string &key);

/* 保存单个交易变量 */
void save_variable(SQLite::Database &db, const string &key, const string &value);

/* 更新仓位信息 */
void save_position(SQLite::Database &db, const Position &position);

bool compare_trade_books(const map<double, TradeBook> &trade_books, const map<double, TradeBook> &trade_books_last);

/* 输入要分割的字符串和分割的方式 */
vector<string> split(const string &str, const string &pattern);

/* 判断时间戳是否在区间中 */
bool check_time_in_range(time_t now, const string &begin, const string &end);

/* 判断时间戳是否在多个时间段内 */
/* "trade_time": ["09:00:05","14:59:55","21:00:05","22:59:55"] */
bool check_time_in_periods(const time_t &now, const vector<string> &time_periods);

/* 判断时间戳是否临近结束 */
bool check_time_near_end(time_t now, int seconds, const string &end);

/* 成交后更新仓位 */
void update_position_by_trade(Position &position, bool isLong, bool isOpen, int volume);

/* 兼容wt和kungfu对于交易方向的判断，通过此函数进行转换
// wt对于isLong有特殊的处理,通过下表反向判断多空队列
// 多    开   ->  true
// 多    平   ->  false
// 空    开   ->  false
// 空    平   ->  true
 */
TThostFtdcDirectionType get_direction_by_islong_and_offset(bool isLong, uint32_t offset);

// 在价差合约代码中解析出主动合约代码和被动合约代码
// 价差合约[SPD RM709&RM801.CZCE]返回主动合约[RM709] 被动合约[RM801]
string parse_init_from_sp_symbol(const string &sp_symbol);

string parse_passive_from_sp_symbol(const string &sp_symbol);

#endif //STRATEGY_COMMON_H