#pragma once

#include <unordered_set>
#include <memory>
#include <thread>
#include <mutex>
#include <vector>
#include <queue>

#include "../Includes/UftStrategyDefs.h"
#include "../Share/SpinMutex.hpp"
#include "common.h"
#include "../Includes/ExecuteDefs.h"
#include "../Share/fmtlib.h"
#include "../Share/TimeUtils.hpp"
#include "../Includes/WTSSessionInfo.hpp"

class WtUftStraPair : public UftStrategy {
public:
    explicit WtUftStraPair(const char *id);

    ~WtUftStraPair() override;

public:
    const char *getName() override;

    const char *getFactName() override;

    bool init(WTSVariant *cfg) override;

    void on_init(IUftStraCtx *ctx) override;

    void on_tick(IUftStraCtx *ctx, const char *code, WTSTickData *newTick) override;

    void on_bar(IUftStraCtx *ctx, const char *code, const char *period, uint32_t times, WTSBarStruct *newBar) override;

    void on_minute_end(IUftStraCtx *ctx, uint32_t curTime) override;

    void on_trade(IUftStraCtx *ctx, uint32_t localid, const char *stdCode,
                  bool isLong, uint32_t offset, double qty, double price) override;

    void on_position(IUftStraCtx *ctx, const char *stdCode, bool isLong,
                     double prevol, double preavail, double newvol, double newavail) override;

    void on_order(IUftStraCtx *ctx, uint32_t localid, const char *stdCode, bool isLong, uint32_t offset,
                  double totalQty, double leftQty, double price, bool isCanceled) override;

    void on_channel_ready(IUftStraCtx *ctx) override;

    void on_channel_lost(IUftStraCtx *ctx) override;

    void on_entrust(uint32_t localid, bool bSuccess, const char *message) override;

    void on_exit(IUftStraCtx *ctx) override;

private:
    WTSTickData *_last_tick;
    IUftStraCtx *_ctx{};
    std::string _code;
    bool _channel_ready;
    WTSSessionInfo *_session_info{nullptr};
private:
    SQLite::Database db;
    bool isStarted{false};
public:
    // 策略参数
    string strategy_name;
    string symbol_init, exchange_init;
    string symbol_passive, exchange_passive;
    double pricetick_init{}, pricetick_passive{}, pricetick_spread{};
    uint32_t step_size{};
    int deviation_adjust_buy{}, deviation_adjust_sell{};
    uint32_t exceed_tick{};
    uint32_t weight{};
    uint32_t max_cancel{};
    bool zero_stop{};
    uint32_t exit_days{};
    int average_adjust{0};
    unsigned int rebuild_book_step{0}; // 0:不重新构建，每次启动重建交易登记簿，向均值移动，单位pricetick

    // 交易变量
    string symbol_sp;
    string vt_symbol_init, vt_symbol_passive;
    string wt_symbol_init, wt_symbol_passive;
    string trade_date;  // 当前交易日期
    uint32_t remaining_days{}; //合约到期剩余天数
    map<double, TradeBook> trade_books{};  // price : TradeBook
    map<double, TradeBook> trade_books_last{};  // price : TradeBook

    bool position_inited{false};
    Position position_sp{}, position_init{}, position_passive{};

    optional<double> price_init_buy{nullopt}, price_init_sell{nullopt};
    optional<int> volume_init_buy{nullopt}, volume_init_sell{nullopt};
    optional<double> price_passive_buy{nullopt}, price_passive_sell{nullopt};
    optional<int> volume_passive_buy{nullopt}, volume_passive_sell{nullopt};

    // 主动合约和被动合约的涨跌停价格
    optional<double> price_init_limit_up{nullopt}, price_init_limit_down{nullopt};
    optional<double> price_passive_limit_up{nullopt}, price_passive_limit_down{nullopt};

    // 虚拟套利合约价格
    optional<double> price_sp_buy{nullopt}, price_sp_buy_last{nullopt};
    optional<double> price_sp_sell{nullopt}, price_sp_sell_last{nullopt};

    // 记录主动合约委托价格,便于后续处理
    double price_init_buy_order{}, price_init_sell_order{}, price_passive_buy_order{}, price_passive_sell_order{};

    // 套利合约价格均值
    optional<double> sp_average_price{nullopt};

    // 委托
    uint32_t orderid_init_buy{0}, orderid_init_sell{0}, orderid_passive_buy{0}, orderid_passive_sell{0};
    uint32_t orderid_init_buy_hedge{0}, orderid_init_sell_hedge{0},
            orderid_passive_buy_hedge{0}, orderid_passive_sell_hedge{0};

    // 成交队列
    queue<TradeData> trade_queue_init_buy, trade_queue_init_sell, trade_queue_passive_buy, trade_queue_passive_sell;
    vector<TradeData> trade_all;

    // 需要保存的策略运行变量
    int cancel_count_init{}, cancel_count_passive{};

    // 保存order和trade，用于校验是否同时成交
    map<uint32_t, OrderData> orders{};   // localid : OrderData
    map<uint32_t, double> trades{}; // localid : volume

    void load_trade_books();

    void print_trade_books();

    void rebuild_tradebook();

    optional<double> get_max_bought();

    optional<double> get_min_bought();

    optional<double> get_min_sold();

    optional<double> get_max_sold();

    TradeBook &get_trade_book(double price);

    void print_tick();

    void print_position();

    void refresh_orders_status();

    bool check_order_all_processed(uint32_t localid);

    void process_trade_queue();

    void hedge();

    void
    put_trade_to_queue(const char *stdCode, uint32_t localid, bool isLong, uint32_t offset, double qty, double price);

    void process_sp_trade(const string &time, double price, bool isLong, uint32_t offset, uint32_t volume);

    uint32_t cal_buy_volume(double buy_price);

    bool is_lowest_bought_of_book(double price);

    uint32_t sum_bought(double price);

    uint32_t cal_sell_volume(double sell_price);

    bool is_highest_sold_of_book(double price);

    uint32_t sum_sold(double price);

    void trade();

    void prepare_buy_order_make(double _price_init_buy, double _price_init_sell, double _price_passive_buy,
                                double _price_passive_sell);

    void prepare_sell_order_make(double _price_init_buy, double _price_init_sell, double _price_passive_buy,
                                 double _price_passive_sell);

    void prepare_sell_order_take(double _price_init_sell, double _price_passive_buy);

    void prepare_buy_order_take(double _price_init_buy, double _price_passive_sell);

    void dump_trade_book();

    void dump_trade_all();

    void dump_variable();

    void dump_position();

    void update_position();
};

