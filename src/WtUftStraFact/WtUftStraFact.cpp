#include "WtUftStraFact.h"
#include "WtUftStraPair.h"
#include "WtUftStraNet.h"

#include <cstring>

const char *FACT_NAME = "WtUftStraFact";

extern "C"
{
EXPORT_FLAG IUftStrategyFact *createStrategyFact() {
    IUftStrategyFact *fact = new WtUftStraFact();
    return fact;
}

EXPORT_FLAG void deleteStrategyFact(IUftStrategyFact *fact) {
    if (fact != NULL)
        delete fact;
}
}


WtUftStraFact::WtUftStraFact() = default;


WtUftStraFact::~WtUftStraFact() = default;

const char *WtUftStraFact::getName() {
    return FACT_NAME;
}

void WtUftStraFact::enumStrategy(FuncEnumUftStrategyCallback cb) {
    cb(FACT_NAME, "WtPair", true);
    cb(FACT_NAME, "WtNet", true);
}

UftStrategy *WtUftStraFact::createStrategy(const char *name, const char *id) {
    if (strcmp(name, "WtPair") == 0) {
        return new WtUftStraPair(id);
    }
    if (strcmp(name, "WtNet") == 0) {
        return new WtUftStraNet(id);
    }
    return NULL;
}

bool WtUftStraFact::deleteStrategy(UftStrategy *stra) {
    if (stra == NULL)
        return true;

    if (strcmp(stra->getFactName(), FACT_NAME) != 0)
        return false;

    delete stra;
    return true;
}
