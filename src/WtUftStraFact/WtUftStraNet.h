#pragma once

#include <unordered_set>
#include <memory>
#include <thread>
#include <mutex>
#include <vector>
#include <queue>


#include "../Includes/UftStrategyDefs.h"
#include "../Share/SpinMutex.hpp"
#include "common.h"
#include "../Includes/ExecuteDefs.h"
#include "../Share/fmtlib.h"
#include "../Share/TimeUtils.hpp"
#include "../Includes/WTSSessionInfo.hpp"

class WtUftStraNet : public UftStrategy {
public:
    explicit WtUftStraNet(const char *id);

    ~WtUftStraNet() override;

public:
    const char *getName() override;

    const char *getFactName() override;

    bool init(WTSVariant *cfg) override;

    void on_init(IUftStraCtx *ctx) override;

    void on_tick(IUftStraCtx *ctx, const char *code, WTSTickData *newTick) override;

    void on_bar(IUftStraCtx *ctx, const char *code, const char *period, uint32_t times, WTSBarStruct *newBar) override;

    void on_minute_end(IUftStraCtx *ctx, uint32_t curTime) override;

    void on_trade(IUftStraCtx *ctx, uint32_t localid, const char *stdCode, bool isLong, uint32_t offset, double qty,
                  double price) override;

    void on_position(IUftStraCtx *ctx, const char *stdCode, bool isLong, double prevol, double preavail, double newvol,
                     double newavail) override;

    void on_order(IUftStraCtx *ctx, uint32_t localid, const char *stdCode, bool isLong, uint32_t offset,
                  double totalQty, double leftQty, double price, bool isCanceled) override;

    void on_channel_ready(IUftStraCtx *ctx) override;

    void on_channel_lost(IUftStraCtx *ctx) override;

    void on_entrust(uint32_t localid, bool bSuccess, const char *message) override;

    void on_exit(IUftStraCtx *ctx) override;

private:
    IUftStraCtx *_ctx{};
    WTSSessionInfo *_session_info{nullptr};
    bool _channel_ready;
    SQLite::Database db;
    bool isStarted{false};
public:
    // 策略参数
    string strategy_name;
    string symbol, exchange;
    double pricetick{};
    unsigned int step_size{};
    int deviation_adjust_buy{}, deviation_adjust_sell{};
    unsigned int weight{};
    bool zero_stop{};
    unsigned int exit_days{};
    int average_adjust{0};
    unsigned int rebuild_book_step{0}; // 0:不重新构建，每次启动重建交易登记簿，向均值移动，单位pricetick

    // 交易变量
    bool is_spread_mode{true};    // 本策略默认交易套利合约，普通合约也能交易
    string vt_symbol, wt_symbol;
    string symbol_init, symbol_passive;
    string vt_symbol_init, vt_symbol_passive, wt_symbol_init, wt_symbol_passive;
    string trade_date;  // 当前交易日期
    uint32_t remaining_days{}; //合约到期剩余天数
    map<double, TradeBook> trade_books{};  // price : TradeBook
    map<double, TradeBook> trade_books_last{};  // price : TradeBook

    bool position_inited{false};
    Position position{};

    optional<double> price_buy{nullopt}, price_sell{nullopt}, price_buy_last{nullopt}, price_sell_last{nullopt};
    optional<int> volume_buy{nullopt}, volume_sell{nullopt};

    // 主动合约和被动合约的涨跌停价格
    optional<double> price_limit_up{nullopt}, price_limit_down{nullopt};

    // 套利合约价格均值
    optional<double> sp_average_price{nullopt};

    // 委托
    uint32_t orderid_buy{0}, orderid_sell{0};

    // 成交队列
    queue<TradeData> trade_queue_init_buy, trade_queue_init_sell, trade_queue_passive_buy, trade_queue_passive_sell;
    vector<TradeData> trade_all;

    // 保存order和trade，用于校验是否同时成交
    map<uint32_t, OrderData> orders{};   // localid : OrderData
    map<uint32_t, double> trades{}; // localid : volume

    void load_trade_books();

    void print_trade_books();

    void rebuild_tradebook();

    optional<double> get_max_bought();

    optional<double> get_min_bought();

    optional<double> get_min_sold();

    optional<double> get_max_sold();

    TradeBook &get_trade_book(double price);

    void print_tick();

    void update_position();

    void print_position();

    uint32_t cal_buy_volume(double buy_price);

    bool is_lowest_bought_of_book(double price);

    uint32_t sum_bought(double price);

    uint32_t cal_sell_volume(double _price_sell);

    bool is_highest_sold_of_book(double price);

    uint32_t sum_sold(double price);

    void trade_depend_tick();

    void trade_depend_book();

    void prepare_sell_order(double sell_price);

    void prepare_buy_order(double buy_price);

    void dump_trade_book();

    void dump_trade_all();

    void dump_position();
};
