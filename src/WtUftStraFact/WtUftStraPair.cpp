#include "WtUftStraPair.h"
#include "../Includes/IUftStraCtx.h"

#include "../Includes/WTSVariant.hpp"
#include "../Includes/WTSContractInfo.hpp"
#include "../Share/decimal.h"


extern const char *FACT_NAME;

WtUftStraPair::WtUftStraPair(const char *id)
        : UftStrategy(id), _last_tick(nullptr), _channel_ready(false),
          db(string(id) + ".db", SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE) {
    strategy_name = id;
}


WtUftStraPair::~WtUftStraPair() {
    if (_last_tick)
        _last_tick->release();
}

const char *WtUftStraPair::getName() {
    return "UftDemoStrategy";
}

const char *WtUftStraPair::getFactName() {
    return FACT_NAME;
}

bool WtUftStraPair::init(WTSVariant *cfg) {
    //这里演示一下外部传入参数的获取
    symbol_init = cfg->getCString("symbol_init");
    exchange_init = cfg->getCString("exchange_init");
    pricetick_init = cfg->getDouble("pricetick_init");
    symbol_passive = cfg->getCString("symbol_passive");
    exchange_passive = cfg->getCString("exchange_passive");
    pricetick_passive = cfg->getDouble("pricetick_passive");
    pricetick_spread = cfg->getDouble("pricetick_spread");
    step_size = cfg->getUInt32("step_size");
    deviation_adjust_buy = cfg->getInt32("deviation_adjust_buy");
    deviation_adjust_sell = cfg->getInt32("deviation_adjust_sell");
    exceed_tick = cfg->getUInt32("exceed_tick");
    weight = cfg->getUInt32("weight");
    max_cancel = cfg->getUInt32("max_cancel");
    zero_stop = cfg->getBoolean("zero_stop");
    exit_days = cfg->getUInt32("exit_days");
    average_adjust = cfg->getInt32("average_adjust");
    rebuild_book_step = cfg->getUInt32("rebuild_book_step");
    return true;
}

void WtUftStraPair::on_entrust(uint32_t localid, bool bSuccess, const char *message) {
    if (!bSuccess) {
        if (localid == orderid_init_buy) {
            orderid_init_buy = 0;
            price_init_buy_order = 0;
        }
        if (localid == orderid_init_sell) {
            orderid_init_sell = 0;
            price_init_sell_order = 0;
        }
        if (localid == orderid_passive_buy) {
            orderid_passive_buy = 0;
            price_passive_buy_order = 0;
        }
        if (localid == orderid_passive_sell) {
            orderid_passive_sell = 0;
            price_passive_sell_order = 0;
        }
        // hedge orderid
        if (localid == orderid_init_buy_hedge) orderid_init_buy_hedge = 0;
        if (localid == orderid_init_sell_hedge) orderid_init_sell_hedge = 0;
        if (localid == orderid_passive_buy_hedge) orderid_passive_buy_hedge = 0;
        if (localid == orderid_passive_sell_hedge) orderid_passive_sell_hedge = 0;
        // print my entrust error
        if (localid == orderid_init_buy || localid == orderid_init_sell ||
            localid == orderid_passive_buy || localid == orderid_passive_sell ||
            localid == orderid_init_buy_hedge || localid == orderid_init_sell_hedge ||
            localid == orderid_passive_buy_hedge || localid == orderid_passive_sell_hedge) {
            _ctx->stra_log_info(fmt::format("on_entrust fail [{}],error[{}]:", localid, message).c_str());
        }
    }
}

// 不依赖行情和交易服务连接的初始化
void WtUftStraPair::on_init(IUftStraCtx *ctx) {
    ctx->stra_log_info(fmt::format("on_init id[{}] name[{}]", ctx->id(), ctx->name()).c_str());
    _ctx = ctx;
    _ctx->stra_log_info(fmt::format("strategy [{}] init, params:", strategy_name).c_str());
    _ctx->stra_log_info(fmt::format("symbol_init:{},exchange_init:{},pricetick_init:{}",
                                    symbol_init, exchange_init, pricetick_init).c_str());
    _ctx->stra_log_info(fmt::format("symbol_passive:{},exchange_passive:{},pricetick_passive:{}",
                                    symbol_passive, exchange_passive, pricetick_passive).c_str());
    _ctx->stra_log_info(fmt::format("pricetick_spread: {}", pricetick_spread).c_str());
    _ctx->stra_log_info(fmt::format("step_size: {}", step_size).c_str());
    _ctx->stra_log_info(fmt::format("deviation_adjust_buy: {}", deviation_adjust_buy).c_str());
    _ctx->stra_log_info(fmt::format("deviation_adjust_sell: {}", deviation_adjust_sell).c_str());
    _ctx->stra_log_info(fmt::format("exceed_tick: {}", exceed_tick).c_str());
    _ctx->stra_log_info(fmt::format("weight: {}", weight).c_str());
    _ctx->stra_log_info(fmt::format("max_cancel: {}", max_cancel).c_str());
    _ctx->stra_log_info(fmt::format("zero_stop: {}", zero_stop).c_str());
    _ctx->stra_log_info(fmt::format("exit_days: {}", exit_days).c_str());
    _ctx->stra_log_info(fmt::format("rebuild_book_step: {}", rebuild_book_step).c_str());
    _ctx->stra_log_info(fmt::format("average_adjust: {}", average_adjust).c_str());

    // 交易变量
    symbol_sp = symbol_init + "_" + symbol_passive;
    vt_symbol_init = symbol_init + "." + exchange_init;
    vt_symbol_passive = symbol_passive + "." + exchange_passive;
    wt_symbol_init = exchange_init + "." + symbol_init;
    wt_symbol_passive = exchange_passive + "." + symbol_passive;
    trade_date = get_trade_date();
    // 合约剩余天数
    remaining_days = min(get_remaining_days(trade_date, symbol_init),
                         get_remaining_days(trade_date, symbol_passive));
    _ctx->stra_log_info(fmt::format("trade_date:{} remaining_days:{}", trade_date, remaining_days).c_str());
    // 取交易时间段
    WTSCommodityInfo *_comm_info = ctx->stra_get_comminfo(wt_symbol_init.c_str());
    _session_info = _comm_info->getSessionInfo();
    _ctx->stra_log_info(fmt::format("Session begin:{} end:{}",
                                    _session_info->getOpenTime(), _session_info->getCloseTime()).c_str());
    // 计算合约均价
    optional<double> average_price_sp_old = get_average_price_pair(vt_symbol_init, vt_symbol_passive, trade_date);
    if (average_price_sp_old == nullopt) {
        _ctx->stra_log_info("get_average_price_pair fail,strategy end.");
        exit(1);
    }
    sp_average_price = round_to_pricetick(average_price_sp_old.value() + float(average_adjust) * pricetick_spread,
                                          pricetick_spread);
    _ctx->stra_log_info(fmt::format("original average price:{} adjust:{} new:{}",
                                    average_price_sp_old.value(), average_adjust, sp_average_price.value()).c_str());
    _ctx->stra_log_info("----------trading data loading begin----------");
    // 初始化数据库,建表
    initDatabase(strategy_name + ".db");
    // 载入数据库存储交易变量
    string v_trade_date = load_variable(db, "trade_date");
    if (v_trade_date == trade_date) {
        string v_cancel_count_init = load_variable(db, "cancel_count_init");
        if (v_cancel_count_init.empty()) cancel_count_init = 0;
        else cancel_count_init = stoi(v_cancel_count_init);
        string v_cancel_count_passive = load_variable(db, "cancel_count_passive");
        if (v_cancel_count_passive.empty()) cancel_count_passive = 0;
        else cancel_count_passive = stoi(v_cancel_count_passive);
    } else {
        save_variable(db, "trade_date", trade_date);
        cancel_count_init = 0;
        save_variable(db, "cancel_count_init", "0");
        cancel_count_passive = 0;
        save_variable(db, "cancel_count_passive", "0");
    }
    _ctx->stra_log_info(fmt::format("trading vars: cancel_count_init:{} cancel_count_passive:{}",
                                    cancel_count_init, cancel_count_passive).c_str());
    // 获取当前委托登记薄
    load_trade_books();
    print_trade_books();
    // 重新构建tradebooks
    if (rebuild_book_step > 0) rebuild_tradebook();
    _ctx->stra_log_info("----------trading data loading end----------");
    isStarted = true;
}

void WtUftStraPair::load_trade_books() {
    trade_books.clear();
    SQLite::Statement query(db, "SELECT * FROM tradebook");
    while (query.executeStep()) {
        TradeBook book = {
                query.getColumn(0),
                query.getColumn(1),
                0,
                0,
                query.getColumn(4),
                query.getColumn(5)};
        trade_books.insert(pair<double, TradeBook>(query.getColumn(1), book));
    }
}

void WtUftStraPair::print_trade_books() {
    if (CompareTradeBooks(trade_books, trade_books_last)) return;
    _ctx->stra_log_info("----- trade books -----");
    uint32_t sumBuy{0}, sumSell{0}, sumBought{0}, sumSold{0};
    for (auto rit = trade_books.rbegin(); rit != trade_books.rend(); rit++) {
        const TradeBook &b = rit->second;
        sumBuy += b.buy;
        sumSell += b.sell;
        sumBought += b.bought;
        sumSold += b.sold;
        if (b.buy != 0 || b.sell != 0 || b.bought != 0 || b.sold != 0) {
            _ctx->stra_log_info(fmt::format("{}\t{}\t{}\t{}\t{}", b.price, b.buy, b.sell, b.bought, b.sold).c_str());
        }
    }
    _ctx->stra_log_info(fmt::format("book sum: {}  {} & pos {}  {}",
                                    sumBought, sumSold, position_sp.pos_long, position_sp.pos_short).c_str());
    trade_books_last = trade_books;
}

void WtUftStraPair::rebuild_tradebook() {
    if (trade_books.empty()) return;
    // process bought
    optional<double> max_bought = get_max_bought();
    if (max_bought != nullopt && (max_bought > sp_average_price || remaining_days < exit_days)) {
        // 从小到大处理
        for (auto &[k, v]: trade_books) {
            if (v.bought < 1) continue;
            else {
                v.bought -= 1;
                TradeBook &new_bought_book = get_trade_book(k - pricetick_spread * float(rebuild_book_step));
                new_bought_book.bought += 1;
            }
        }
    }
    // process sold
    optional<double> min_sold = get_min_sold();
    if (min_sold != nullopt && (min_sold < sp_average_price || remaining_days < exit_days)) {
        // 从大到小处理
        for (auto rit = trade_books.rbegin(); rit != trade_books.rend(); rit++) {
            TradeBook &b = rit->second;
            if (b.sold < 1) continue;
            else {
                b.sold -= 1;
                TradeBook &new_sold_book = get_trade_book(b.price + pricetick_spread * float(rebuild_book_step));
                new_sold_book.sold += 1;
            }
        }
    }
    _ctx->stra_log_info("----------after rebuild, new trade books----------");
    print_trade_books();
}

// 取book中最大买入价，如果没有返回nullopt
optional<double> WtUftStraPair::get_max_bought() {
    optional<double> p;
    for (const auto &[_, b]: trade_books) {
        if (b.bought >= 1 && (p == nullopt || b.price > p)) p = b.price;
    }
    return p;
}

// 取book中最小买入价，如果没有返回nullopt
optional<double> WtUftStraPair::get_min_bought() {
    optional<double> p;
    for (const auto &[_, b]: trade_books) {
        if (b.bought >= 1 && (p == nullopt || b.price < p)) p = b.price;
    }
    return p;
}

// 取book中最小卖出价，如果没有返回nullopt
optional<double> WtUftStraPair::get_min_sold() {
    optional<double> p;
    for (const auto &[_, b]: trade_books) {
        if (b.sold >= 1 && (p == nullopt || b.price < p)) p = b.price;
    }
    return p;
}

// 取book中最大卖出价，如果没有返回nullopt
optional<double> WtUftStraPair::get_max_sold() {
    optional<double> p;
    for (const auto &[_, b]: trade_books) {
        if (b.sold >= 1 && (p == nullopt || b.price > p)) p = b.price;
    }
    return p;
}

/*
	判断价格是否存在交易登记簿中，
	有则直接返回，无则新建空项目返回
 */
TradeBook &WtUftStraPair::get_trade_book(double price) {
    double new_price = round_to_pricetick(price, pricetick_spread);
    auto iter = trade_books.find(new_price);
    if (iter != trade_books.end()) {
        return iter->second;
    } else {
        trade_books[new_price] = TradeBook{strategy_name, new_price, 0, 0, 0, 0};
        return trade_books[new_price];
    }
}


/* 打印tick数据 */
void WtUftStraPair::print_tick() {
    if (price_sp_buy != price_sp_buy_last || price_sp_sell != price_sp_sell_last) {
        _ctx->stra_log_info(
                fmt::format("init buy={} {} ({}), sell={} {} ({}) | passive buy={} {} ({}), sell={} {} ({})",
                            price_init_buy.value(), volume_init_buy.value(), orderid_init_buy,
                            price_init_sell.value(), volume_init_sell.value(), orderid_init_sell,
                            price_passive_buy.value(), volume_passive_buy.value(), orderid_passive_buy,
                            price_passive_sell.value(), volume_passive_sell.value(),
                            orderid_passive_sell).c_str());
        double price_sp_buy_take = round_to_pricetick(price_init_sell.value() - price_passive_buy.value(),
                                                      pricetick_spread);
        double price_sp_buy_make_init = round_to_pricetick(price_init_buy.value() - price_passive_buy.value(),
                                                           pricetick_spread);
        double price_sp_buy_make_passive = round_to_pricetick(price_init_sell.value() - price_passive_sell.value(),
                                                              pricetick_spread);
        double price_sp_sell_take = round_to_pricetick(price_init_buy.value() - price_passive_sell.value(),
                                                       pricetick_spread);
        double price_sp_sell_make_init = round_to_pricetick(price_init_sell.value() - price_passive_sell.value(),
                                                            pricetick_spread);
        double price_sp_sell_make_passive = round_to_pricetick(price_init_buy.value() - price_passive_buy.value(),
                                                               pricetick_spread);
        _ctx->stra_log_info(fmt::format("Spread buy={} {} {}, sell={} {} {} | Avg:{}",
                                        price_sp_buy_take, price_sp_buy_make_init, price_sp_buy_make_passive,
                                        price_sp_sell_take, price_sp_sell_make_init, price_sp_sell_make_passive,
                                        sp_average_price.value()).c_str());
        price_sp_buy_last = price_sp_buy;
        price_sp_sell_last = price_sp_sell;
    }
}


void WtUftStraPair::on_tick(IUftStraCtx *ctx, const char *code, WTSTickData *newTick) {
    // _ctx->stra_log_info(fmt::format("on_tick id[{}] name[{}]", ctx->id(), ctx->name()).c_str());
    if (!_channel_ready || !isStarted) return;
    //初始化仓位
    if (!position_inited) update_position();
    // 过滤非本策略回报
    if (code != wt_symbol_init && code != wt_symbol_passive) {
        _ctx->stra_log_error(fmt::format("other symbol: {}", code).c_str());
        return;
    }
    // 策略停止判断
    if (zero_stop && position_sp.pos_long == 0 && position_sp.pos_short == 0) {
        _ctx->stra_cancel_all(wt_symbol_init.c_str());
        _ctx->stra_cancel_all(wt_symbol_passive.c_str());
        _ctx->stra_log_info("Zero_stop is true and position is empty, stop running!!!");
        return;
    }
    if (remaining_days < exit_days && position_sp.pos_long == 0 && position_sp.pos_short == 0) {
        _ctx->stra_cancel_all(wt_symbol_init.c_str());
        _ctx->stra_cancel_all(wt_symbol_passive.c_str());
        _ctx->stra_log_info("Contract is Coming to end and position is empty, stop running!!!");
        return;
    }

    // 挂单量为0时表示没人挂单，此时不能按价格0下单
    if (code == wt_symbol_init) {
        if (newTick->bidqty(0) == 0 || !price_valid(newTick->bidprice(0))) {
            price_init_buy = nullopt;
        } else {
            price_init_buy = round_to_pricetick(newTick->bidprice(0), pricetick_init);
            volume_init_buy = newTick->bidqty(0);
        }
        if (newTick->askqty(0) == 0 || !price_valid(newTick->askprice(0))) {
            price_init_sell = nullopt;
        } else {
            price_init_sell = round_to_pricetick(newTick->askprice(0), pricetick_init);
            volume_init_sell = newTick->askqty(0);
        }
        if (!price_init_limit_up.has_value() || !price_init_limit_down.has_value()) {
            price_init_limit_up = newTick->upperlimit();
            price_init_limit_down = newTick->lowerlimit();
            _ctx->stra_log_info(fmt::format("price_init_limit_down:{},price_init_limit_up:{}",
                                            price_init_limit_down.value(), price_init_limit_up.value()).c_str());
        }
    }
    if (code == wt_symbol_passive) {
        if (newTick->bidqty(0) == 0 || !price_valid(newTick->bidprice(0))) {
            price_passive_buy = nullopt;
        } else {
            price_passive_buy = round_to_pricetick(newTick->bidprice(0), pricetick_passive);
            volume_passive_buy = newTick->bidqty(0);
        }
        if (newTick->askqty(0) == 0 || !price_valid(newTick->askprice(0))) {
            price_passive_sell = nullopt;
        } else {
            price_passive_sell = round_to_pricetick(newTick->askprice(0), pricetick_passive);
            volume_passive_sell = newTick->askqty(0);
        }
        if (!price_passive_limit_up.has_value() || !price_passive_limit_down.has_value()) {
            price_passive_limit_up = newTick->upperlimit();
            price_passive_limit_down = newTick->lowerlimit();
            _ctx->stra_log_info(fmt::format("price_passive_limit_down:{}, price_passive_limit_up:{}",
                                            price_passive_limit_down.value(), price_passive_limit_up.value()).c_str());
        }
    }
    if (!(price_init_buy.has_value() && price_init_sell.has_value()
          && price_passive_buy.has_value() && price_passive_sell.has_value())) {
        return;
    }

    // 计算套利合约价格
    if (cancel_count_init < max_cancel) {
        price_sp_buy = round_to_pricetick(price_init_buy.value() - price_passive_buy.value(), pricetick_spread);
        price_sp_sell = round_to_pricetick(price_init_sell.value() - price_passive_sell.value(), pricetick_spread);
    } else {
        price_sp_buy = round_to_pricetick(price_init_sell.value() - price_passive_sell.value(), pricetick_spread);
        price_sp_sell = round_to_pricetick(price_init_buy.value() - price_passive_buy.value(), pricetick_spread);
    }
    print_tick();

    // 委托单的委托和成交处理是否已经结束
    refresh_orders_status();

    // 委托单是否需要撤单
    if (orderid_init_buy != 0 && trade_queue_passive_sell.empty()) {
        double price_sp_buy_dynamic_order = round_to_pricetick(price_init_buy_order - price_passive_buy.value(),
                                                               pricetick_spread);
        uint32_t buy_volume_dynamic_order = cal_buy_volume(price_sp_buy_dynamic_order);
        // 撤单规则一：价差失效，且委托价将被成交
        if (buy_volume_dynamic_order <= 0 && price_init_buy_order >= price_init_buy.value() - pricetick_init) {
            _ctx->stra_log_info(fmt::format("Spread buy [{}] fail, orderid_init_buy[{}][{}]",
                                            price_sp_buy_dynamic_order, orderid_init_buy,
                                            price_init_buy_order).c_str());
            _ctx->stra_cancel(orderid_init_buy);
            cancel_count_init++;
        }
        // 撤单规则二：盘口价符合价差交易，且委托价小于盘口
        double price_sp_buy_dynamic = round_to_pricetick(price_init_buy.value() - price_passive_buy.value(),
                                                         pricetick_spread);
        uint32_t buy_volume_dynamic = cal_buy_volume(price_sp_buy_dynamic);
        if (buy_volume_dynamic > 0 && price_init_buy_order < price_init_buy) {
            _ctx->stra_log_info(fmt::format("Change price_init_buy [{}] -> [{}]",
                                            price_init_buy_order, price_init_buy.value()).c_str());
            _ctx->stra_cancel(orderid_init_buy);
            cancel_count_init++;
        }
    }
    if (orderid_init_sell != 0 && trade_queue_passive_buy.empty()) {
        double price_sp_sell_dynamic_order = round_to_pricetick(price_init_sell_order - price_passive_sell.value(),
                                                                pricetick_spread);
        uint32_t sell_volume_dynamic_order = cal_sell_volume(price_sp_sell_dynamic_order);
        // 撤单规则一：价差失效，且委托价将被成交
        if (sell_volume_dynamic_order <= 0 && price_init_sell_order <= price_init_sell.value() + pricetick_init) {
            _ctx->stra_log_info(fmt::format("Spread sell [{}] fail, orderid_init_sell[{}][{}]",
                                            price_sp_sell_dynamic_order, orderid_init_sell,
                                            price_init_sell_order).c_str());
            _ctx->stra_cancel(orderid_init_sell);
            cancel_count_init++;
        }
        // 撤单规则二：盘口价符合价差交易，且委托价大于盘口卖价
        double price_sp_sell_dynamic = round_to_pricetick(price_init_sell.value() - price_passive_sell.value(),
                                                          pricetick_spread);
        uint32_t sell_volume_dynamic = cal_sell_volume(price_sp_sell_dynamic);
        if (sell_volume_dynamic > 0 && price_init_sell_order > price_init_sell) {
            _ctx->stra_log_info(fmt::format("Change price_init_sell [{}] -> [{}]",
                                            price_init_sell_order, price_init_sell.value()).c_str());
            _ctx->stra_cancel(orderid_init_sell);
            cancel_count_init++;
        }
    }
    if (orderid_passive_buy != 0 && trade_queue_init_sell.empty()) {
        double price_sp_sell_dynamic_order = round_to_pricetick(price_init_buy.value() - price_passive_buy_order,
                                                                pricetick_spread);
        uint32_t sell_volume_dynamic_order = cal_sell_volume(price_sp_sell_dynamic_order);
        // 撤单规则一：价差失效，且委托价将被成交
        if (sell_volume_dynamic_order <= 0 &&
            price_passive_buy_order >= price_passive_buy.value() - pricetick_passive) {
            _ctx->stra_log_info(fmt::format("Spread sell [{}] fail, orderid_passive_buy[{}][{}]",
                                            price_sp_sell_dynamic_order, orderid_passive_buy,
                                            price_passive_buy_order).c_str());
            _ctx->stra_cancel(orderid_passive_buy);
            cancel_count_passive++;
        }
        // 撤单规则二：盘口价符合价差交易，且委托价小于盘口
        double price_sp_sell_dynamic = round_to_pricetick(price_init_buy.value() - price_passive_buy.value(),
                                                          pricetick_spread);
        uint32_t sell_volume_dynamic = cal_sell_volume(price_sp_sell_dynamic);
        if (sell_volume_dynamic > 0 && price_passive_buy_order < price_passive_buy) {
            _ctx->stra_log_info(fmt::format("Change price_passive_buy [{}] -> [{}]",
                                            price_passive_buy_order, price_passive_buy.value()).c_str());
            _ctx->stra_cancel(orderid_passive_buy);
            cancel_count_passive++;
        }
    }
    if (orderid_passive_sell != 0 && trade_queue_init_buy.empty()) {
        double price_sp_buy_dynamic_order = round_to_pricetick(price_init_sell.value() - price_passive_sell_order,
                                                               pricetick_spread);
        uint32_t buy_volume_dynamic_order = cal_buy_volume(price_sp_buy_dynamic_order);
        // 撤单规则一：价差失效，且委托价将被成交
        if (buy_volume_dynamic_order <= 0 &&
            price_passive_sell_order <= price_passive_sell.value() + pricetick_passive) {
            _ctx->stra_log_info(fmt::format("Spread buy [{}] fail, orderid_passive_sell[{}][{}]",
                                            price_sp_buy_dynamic_order, orderid_passive_sell,
                                            price_passive_sell_order).c_str());
            _ctx->stra_cancel(orderid_passive_sell);
            cancel_count_passive++;
        }
        // 撤单规则二：盘口价符合价差交易，且委托价大于盘口卖价
        double price_sp_buy_dynamic = round_to_pricetick(price_init_sell.value() - price_passive_sell.value(),
                                                         pricetick_spread);
        uint32_t buy_volume_dynamic = cal_buy_volume(price_sp_buy_dynamic);
        if (buy_volume_dynamic > 0 && price_passive_sell_order > price_passive_sell) {
            _ctx->stra_log_info(fmt::format("Change price_passive_sell [{}] -> [{}]",
                                            price_passive_sell_order, price_passive_sell.value()).c_str());
            _ctx->stra_cancel(orderid_passive_sell);
            cancel_count_passive++;
        }
    }

    // 交易
    trade();
    // 对冲
    hedge();
    print_trade_books();
}


void WtUftStraPair::trade() {
    // 确保对冲完毕后再交易
    if (!(trade_queue_init_buy.empty() && trade_queue_init_sell.empty()
          && trade_queue_passive_buy.empty() && trade_queue_passive_sell.empty())) {
        _ctx->stra_log_info(
                fmt::format("trade_queue not empty. init_buy={} init_sell={}, passive_buy={} passive_sell={}",
                            trade_queue_init_buy.size(), trade_queue_init_sell.size(),
                            trade_queue_passive_buy.size(), trade_queue_passive_sell.size()).c_str());
        return;
    }

    // -- -- -- -- --买入逻辑-- -- -- -- --
    // 对价买入
    if (orderid_init_buy == 0 && orderid_passive_sell == 0
        && orderid_init_sell_hedge == 0 && orderid_passive_sell_hedge == 0
        && price_init_sell.has_value() && price_passive_buy.has_value()) {
        prepare_buy_order_take(price_init_sell.value(), price_passive_buy.value());
    }
    // 加价买入
    if (orderid_init_buy == 0 && orderid_passive_sell == 0
        && orderid_init_sell_hedge == 0 && orderid_passive_sell_hedge == 0
        && price_init_buy.has_value() && price_init_sell.has_value()
        && price_passive_buy.has_value() && price_passive_sell.has_value()) {
        prepare_buy_order_make(price_init_buy.value() + double(exceed_tick) * pricetick_init,
                               price_init_sell.value(), price_passive_buy.value(),
                               price_passive_sell.value() - double(exceed_tick) * pricetick_passive);
    }
    // 排队买入
    if (orderid_init_buy == 0 && orderid_passive_sell == 0
        && orderid_init_sell_hedge == 0 && orderid_passive_sell_hedge == 0
        && price_init_buy.has_value() && price_init_sell.has_value()
        && price_passive_buy.has_value() && price_passive_sell.has_value()) {
        prepare_buy_order_make(price_init_buy.value(), price_init_sell.value(),
                               price_passive_buy.value(), price_passive_sell.value());
    }

    // -- -- -- -- --卖出逻辑-- -- -- -- --
    // 对价卖出
    if (orderid_init_sell == 0 && orderid_passive_buy == 0
        && orderid_init_sell_hedge == 0 && orderid_passive_buy_hedge == 0
        && price_init_buy.has_value() && price_passive_sell.has_value()) {
        prepare_sell_order_take(price_init_buy.value(), price_passive_sell.value());
    }
    // 降价卖出
    if (orderid_init_sell == 0 && orderid_passive_buy == 0
        && orderid_init_sell_hedge == 0 && orderid_passive_buy_hedge == 0
        && price_init_buy.has_value() && price_init_sell.has_value()
        && price_passive_buy.has_value() && price_passive_sell.has_value()) {
        prepare_sell_order_make(price_init_buy.value(),
                                price_init_sell.value() - double(exceed_tick) * pricetick_init,
                                price_passive_buy.value() + double(exceed_tick) * pricetick_passive,
                                price_passive_sell.value());
    }
    // 排队卖出
    if (orderid_init_sell == 0 && orderid_passive_buy == 0
        && orderid_init_sell_hedge == 0 && orderid_passive_buy_hedge == 0
        && price_init_buy.has_value() && price_init_sell.has_value()
        && price_passive_buy.has_value() && price_passive_sell.has_value()) {
        prepare_sell_order_make(price_init_buy.value(), price_init_sell.value(),
                                price_passive_buy.value(), price_passive_sell.value());
    }
}


void WtUftStraPair::prepare_buy_order_take(double _price_init_buy, double _price_passive_sell) {
    double _price_sp_buy = round_to_pricetick(_price_init_buy - _price_passive_sell, pricetick_spread);
    uint32_t buy_volume = cal_buy_volume(_price_sp_buy);
    if (buy_volume >= 1) {
        _ctx->stra_log_info(fmt::format("prepare_buy_order_take [{}] [{}]",
                                        _price_sp_buy, buy_volume).c_str());
        double exceed_price_buy = _price_init_buy + double(exceed_tick) * pricetick_init;
        orderid_init_buy = _ctx->stra_buy(wt_symbol_init.c_str(), exceed_price_buy, buy_volume, 1);
        price_init_buy_order = exceed_price_buy;
        _ctx->stra_log_info(fmt::format("init_buy [{}] [{}] [{}] [{}]",
                                        orderid_init_buy, symbol_init, price_init_buy_order, buy_volume).c_str());

        double exceed_price_sell = _price_passive_sell - double(exceed_tick) * pricetick_passive;
        orderid_passive_sell = _ctx->stra_sell(wt_symbol_passive.c_str(), exceed_price_sell, buy_volume, 1);
        price_passive_sell_order = exceed_price_sell;
        _ctx->stra_log_info(fmt::format("passive_sell [{}] [{}] [{}] [{}]", orderid_passive_sell,
                                        symbol_passive, price_passive_sell_order, buy_volume).c_str());
    }
}

void WtUftStraPair::prepare_sell_order_take(double _price_init_sell, double _price_passive_buy) {
    double _price_sp_sell = round_to_pricetick(_price_init_sell - _price_passive_buy, pricetick_spread);
    uint32_t sell_volume = cal_sell_volume(_price_sp_sell);
    if (sell_volume >= 1) {
        _ctx->stra_log_info(fmt::format("prepare_sell_order_take [{}] [{}]", _price_init_sell, sell_volume).c_str());
        double exceed_price_sell = _price_init_sell - double(exceed_tick) * pricetick_passive;
        orderid_init_sell = _ctx->stra_sell(wt_symbol_init.c_str(), exceed_price_sell, sell_volume, 1);
        price_init_sell_order = exceed_price_sell;
        _ctx->stra_log_info(fmt::format("init_sell [{}] [{}] [{}] [{}]",
                                        orderid_init_sell, symbol_init, price_init_sell_order, sell_volume).c_str());

        double exceed_price_buy = _price_passive_buy + double(exceed_tick) * pricetick_init;
        orderid_passive_buy = _ctx->stra_buy(wt_symbol_passive.c_str(), exceed_price_buy, sell_volume, 1);
        price_passive_buy_order = exceed_price_buy;
        _ctx->stra_log_info(fmt::format("passive_buy [{}] [{}] [{}] [{}]", orderid_passive_buy,
                                        symbol_passive, price_passive_buy_order, sell_volume).c_str());
    }
}

void WtUftStraPair::prepare_buy_order_make(double _price_init_buy, double _price_init_sell,
                                           double _price_passive_buy, double _price_passive_sell) {
    if (cancel_count_init < max_cancel) {
        double _price_sp_buy = round_to_pricetick(_price_init_buy - _price_passive_buy, pricetick_spread);
        uint32_t buy_volume = cal_buy_volume(_price_sp_buy);
        if (buy_volume >= 1) {
            _ctx->stra_log_info(fmt::format("prepare_buy_order_make_init [{}] [{}]",
                                            _price_sp_buy, buy_volume).c_str());
            orderid_init_buy = _ctx->stra_buy(wt_symbol_init.c_str(), _price_init_buy, buy_volume, 0);
            price_init_buy_order = _price_init_buy;
            _ctx->stra_log_info(fmt::format("init_buy [{}] [{}] [{}] [{}]", orderid_init_buy,
                                            symbol_init, price_init_buy_order, buy_volume).c_str());
            return;
        }
    }
    if (cancel_count_passive < max_cancel) {
        double _price_sp_buy = round_to_pricetick(_price_init_sell - _price_passive_sell, pricetick_spread);
        uint32_t buy_volume = cal_buy_volume(_price_sp_buy);
        if (buy_volume >= 1) {
            _ctx->stra_log_info(fmt::format("prepare_buy_order_make_passive [{}] [{}]",
                                            _price_sp_buy, buy_volume).c_str());
            orderid_passive_sell = _ctx->stra_sell(wt_symbol_passive.c_str(), _price_passive_sell, buy_volume, 0);
            price_passive_sell_order = _price_passive_sell;
            _ctx->stra_log_info(fmt::format("passive_sell [{}] [{}] [{}] [{}]", orderid_passive_sell,
                                            symbol_init, price_passive_sell_order, buy_volume).c_str());
            return;
        }
    }
}

void WtUftStraPair::prepare_sell_order_make(double _price_init_buy, double _price_init_sell,
                                            double _price_passive_buy, double _price_passive_sell) {
    if (cancel_count_init < max_cancel) {
        double _price_sp_sell = round_to_pricetick(_price_init_sell - _price_passive_sell, pricetick_spread);
        uint32_t sell_volume = cal_sell_volume(_price_sp_sell);
        if (sell_volume >= 1) {
            _ctx->stra_log_info(fmt::format("prepare_sell_order_make_init [{}] [{}]",
                                            _price_sp_sell, sell_volume).c_str());
            orderid_init_sell = _ctx->stra_sell(wt_symbol_init.c_str(), _price_init_sell, sell_volume, 0);
            price_init_sell_order = _price_init_sell;
            _ctx->stra_log_info(fmt::format("init_sell [{}] [{}] [{}] [{}]", orderid_init_sell,
                                            symbol_init, price_init_sell_order, sell_volume).c_str());
            return;
        }
    }
    if (cancel_count_passive < max_cancel) {
        double _price_sp_sell = round_to_pricetick(_price_init_buy - _price_passive_buy, pricetick_spread);
        uint32_t sell_volume = cal_sell_volume(_price_sp_sell);
        if (sell_volume >= 1) {
            _ctx->stra_log_info(fmt::format("prepare_sell_order_make_passive [{}] [{}]",
                                            _price_sp_sell, sell_volume).c_str());
            orderid_passive_buy = _ctx->stra_buy(wt_symbol_passive.c_str(), _price_passive_buy, sell_volume, 0);
            price_passive_buy_order = _price_passive_buy;
            _ctx->stra_log_info(fmt::format("init_sell [{}] [{}] [{}] [{}]", orderid_init_sell,
                                            symbol_init, price_init_sell_order, sell_volume).c_str());
            return;
        }
    }
}


uint32_t WtUftStraPair::cal_sell_volume(double sell_price) {
    uint32_t v = 0;
    // 上涨阶段可卖出开仓
    if (sell_price >= sp_average_price && is_highest_sold_of_book(sell_price)) {
        v = weight;
        v += int((sell_price - sp_average_price.value()) / (deviation_adjust_sell * pricetick_spread));
    } else {
        // 下跌阶段只能对冲价低买入
        for (const auto &[_, b]: trade_books) {
            if (b.bought < 1 || b.price > sell_price - double(step_size) * pricetick_spread) {
                continue;
            } else {
                v += b.bought;
            }
        }
    }
    // 减去已卖
    uint32_t _sum_sold = sum_sold(sell_price);
    if (v > _sum_sold)return v - _sum_sold;
    else return 0;
}

uint32_t WtUftStraPair::sum_sold(double price) {
    uint32_t sold = 0;
    for (const auto &[_, b]: trade_books) {
        if (b.price > (price - double(step_size) * pricetick_spread) &&
            b.price < price + double(step_size) * pricetick_spread)
            sold += b.sold;
    }
    return sold;
}

bool WtUftStraPair::is_highest_sold_of_book(double price) {
    function < bool(pair<double, TradeBook>) > is_highest_sold = [price](const pair<double, TradeBook> &p) -> bool {
        return !(p.second.sold > 0 && p.second.price > price);
    };
    return all_of(trade_books.begin(), trade_books.end(), is_highest_sold);
}


uint32_t WtUftStraPair::cal_buy_volume(double buy_price) {
    uint32_t v = 0;
    // 下跌阶段可买入开仓，只能以登记簿最低价买入
    if (buy_price <= sp_average_price && is_lowest_bought_of_book(buy_price)) {
        v = weight;
        v += int((sp_average_price.value() - buy_price) / (deviation_adjust_buy * pricetick_spread));
    } else {
        // 上涨阶段只能对冲价高卖出
        for (const auto &[_, b]: trade_books) {
            if (b.sold >= 1 && b.price >= buy_price + double(step_size) * pricetick_spread) {
                v += b.sold;
            }
        }
    }
    // 减去已买
    uint32_t _sum_bought = sum_bought(buy_price);
    if (v > _sum_bought) return v - _sum_bought;
    else return 0;
}

bool WtUftStraPair::is_lowest_bought_of_book(double price) {
    function < bool(pair<double, TradeBook>) > is_lowest_bought = [price](const pair<double, TradeBook> &p) -> bool {
        return !(p.second.bought > 0 && p.second.price < price);
    };
    return all_of(trade_books.begin(), trade_books.end(), is_lowest_bought);
}

uint32_t WtUftStraPair::sum_bought(double price) {
    uint32_t bought = 0;
    for (const auto &[_, b]: trade_books) {
        if (b.price > (price - double(step_size) * pricetick_spread) &&
            b.price < (price + double(step_size) * pricetick_spread))
            bought += b.bought;
    }
    return bought;
}


void WtUftStraPair::on_bar(IUftStraCtx *ctx, const char *code,
                           const char *period, uint32_t times, WTSBarStruct *newBar) {
    _ctx->stra_log_info("on_bar...");
}

void WtUftStraPair::on_minute_end(IUftStraCtx *ctx, uint32_t curTime) {
    if (curTime == 1015 || curTime == 1130 || curTime == 1500 || curTime == 2300) {
        _ctx->stra_log_info(fmt::format("on_minute_end [{}] now dump data ...", curTime).c_str());
        dump_trade_book();
        dump_variable();
        dump_position();
        dump_trade_all();
    }
}

void WtUftStraPair::on_trade(IUftStraCtx *ctx, uint32_t localid, const char *stdCode, bool isLong, uint32_t offset,
                             double qty, double price) {
    // 过滤非本策略回报
    if (stdCode != wt_symbol_init && stdCode != wt_symbol_passive) {
        // _ctx->stra_log_error(fmt::format("other symbol: {}", code).c_str());
        return;
    }
    _ctx->stra_log_info(fmt::format("on_trade {} {} {} {} isLong:{} offset:{}",
                                    localid, stdCode, price, qty, isLong, offset).c_str());
    // 将单腿成交分发入队列等候处理，并记录成交队列
    put_trade_to_queue(stdCode, localid, isLong, offset, qty, price);
    // 根据成交队列匹配生成套利合约成交记录
    process_trade_queue();
    // 判断订单是否结束
    refresh_orders_status();
    // 若存在单腿成交，继续对冲
    hedge();
}

/* 根据成交队列匹配生成套利合约成交记录 */
void WtUftStraPair::process_trade_queue() {
    // 先对冲砍腿
    while (!trade_queue_init_buy.empty() && !trade_queue_init_sell.empty()) {
        TradeData &_trade_init_buy = trade_queue_init_buy.front();
        TradeData &_trade_init_sell = trade_queue_init_sell.front();
        uint32_t common_volume = min(_trade_init_buy.qty, _trade_init_sell.qty);
        _trade_init_buy.qty -= common_volume;
        _trade_init_sell.qty -= common_volume;
        if (_trade_init_buy.qty == 0) trade_queue_init_buy.pop();
        if (_trade_init_sell.qty == 0) trade_queue_init_sell.pop();
    }
    while (!trade_queue_passive_buy.empty() && !trade_queue_passive_sell.empty()) {
        TradeData &_trade_passive_buy = trade_queue_passive_buy.front();
        TradeData &_trade_passive_sell = trade_queue_passive_sell.front();
        uint32_t common_volume = min(_trade_passive_buy.qty, _trade_passive_sell.qty);
        _trade_passive_buy.qty -= common_volume;
        _trade_passive_sell.qty -= common_volume;
        if (_trade_passive_buy.qty == 0) trade_queue_passive_buy.pop();
        if (_trade_passive_sell.qty == 0) trade_queue_passive_sell.pop();
    }

    // 再对冲补腿
    while (!trade_queue_init_buy.empty() && !trade_queue_passive_sell.empty()) {
        TradeData &_trade_init = trade_queue_init_buy.front();
        TradeData &_trade_passive = trade_queue_passive_sell.front();
        double _price = round_to_pricetick(_trade_init.price - _trade_passive.price, pricetick_spread);
        uint32_t common_volume = min(_trade_init.qty, _trade_passive.qty);
        if (common_volume > 0)
            process_sp_trade(_trade_passive.time, _price, _trade_init.isLong, _trade_init.offset, common_volume);
        _trade_init.qty -= common_volume;
        _trade_passive.qty -= common_volume;
        if (_trade_init.qty == 0) trade_queue_init_buy.pop();
        if (_trade_passive.qty == 0) trade_queue_passive_sell.pop();
    }
    while (!trade_queue_init_sell.empty() && !trade_queue_passive_buy.empty()) {
        TradeData &_trade_init = trade_queue_init_sell.front();
        TradeData &_trade_passive = trade_queue_passive_buy.front();
        double _price = round_to_pricetick(_trade_init.price - _trade_passive.price, pricetick_spread);
        uint32_t common_volume = min(_trade_init.qty, _trade_passive.qty);
        process_sp_trade(_trade_passive.time, _price, _trade_init.isLong,
                         _trade_init.offset, common_volume);
        _trade_init.qty -= common_volume;
        _trade_passive.qty -= common_volume;
        if (_trade_init.qty == 0) trade_queue_init_sell.pop();
        if (_trade_passive.qty == 0) trade_queue_passive_buy.pop();
    }
}

/* 更新套利仓位*/
void WtUftStraPair::process_sp_trade(const string &time, double price, bool isLong, uint32_t offset, uint32_t volume) {
    bool _isOpen = (offset == 0);
    // 更新订单薄
    TradeBook &trade_book_item = get_trade_book(price);
    if (isLong) {
        if (_isOpen) {
            trade_book_item.bought += volume;
        } else {
            uint32_t buy_volume = volume;
            // 对冲已卖 - 从高到低对冲
            // 注意，套利合约的网格交易是“按高于成交价从低到高对冲”，而单合约配对交易只能从高到低对冲，否则有可能对冲失败
            for (auto riter = trade_books.rbegin(); riter != trade_books.rend(); riter++) {
                TradeBook &b = riter->second;
                if (b.sold >= 1) {
                    uint32_t deal_volume = min(b.sold, buy_volume);
                    b.sold -= deal_volume;
                    buy_volume -= deal_volume;
                    if (buy_volume <= 0) break;
                }
            }
        }
    } else {
        if (_isOpen) {
            trade_book_item.sold += volume;
        } else {
            uint32_t sell_volume = volume;
            // 对冲已买 - 从低到高对冲
            // 注意，套利合约的网格交易是“按低于成交价从高到低对冲”，而单合约配对交易只能从低到高对冲，否则有可能对冲失败
            for (auto &trade_book: trade_books) {
                TradeBook &b = trade_book.second;
                if (b.bought >= 1) {
                    uint32_t deal_volume = min(b.bought, sell_volume);
                    b.bought -= deal_volume;
                    sell_volume -= deal_volume;
                    if (sell_volume <= 0) break;
                }
            }
        }
    }
    // 保存套利合约成交
    _ctx->stra_log_info(fmt::format("sp_trade {} {} {} {}",
                                    price, isLong ? "Buy" : "Sell", decodeOffset(offset), volume).c_str());
    update_position_by_trade(position_sp, isLong, _isOpen, int(volume));
    print_position();
    TradeData _trade{time, strategy_name, price, isLong, offset, volume};
    trade_all.push_back(_trade);
}

void WtUftStraPair::put_trade_to_queue(const char *stdCode, uint32_t localid, bool isLong, uint32_t offset, double qty,
                                       double price) {
    bool _isOpen = (offset == 0);
    // 入待对冲成交队列
    queue<TradeData> *pQueue;
    if (stdCode == wt_symbol_init) {
        if (isLong) {
            pQueue = &trade_queue_init_buy;
            _ctx->stra_log_info(fmt::format("trade_queue_init_buy join:{}", localid).c_str());
        } else {
            pQueue = &trade_queue_init_sell;
            _ctx->stra_log_info(fmt::format("trade_queue_init_sell join:{}", localid).c_str());
        }
        update_position_by_trade(position_init, isLong, _isOpen, int(qty));
    } else if (stdCode == wt_symbol_passive) {
        if (isLong) {
            pQueue = &trade_queue_passive_buy;
            _ctx->stra_log_info(fmt::format("trade_queue_passive_buy join:{}", localid).c_str());
        } else {
            pQueue = &trade_queue_passive_sell;
            _ctx->stra_log_info(fmt::format("trade_queue_passive_sell join:{}", localid).c_str());
        }
        update_position_by_trade(position_passive, isLong, _isOpen, int(qty));
    } else return;

    string _now = TimeUtils::getLocalTime();
    TradeData _trade{_now, stdCode, price, isLong, offset, uint32_t(qty)};
    if (pQueue != nullptr) pQueue->push(_trade);

    // 更新成交记录全集
    trade_all.push_back(_trade);

    // 更新报单成交量
    auto iter = trades.find(localid);
    if (iter != trades.end()) {
        iter->second += qty;
    } else {
        trades[localid] = qty;
    }
}


void WtUftStraPair::hedge() {
    if (!trade_queue_init_buy.empty() && trade_queue_init_sell.empty() && trade_queue_passive_sell.empty()
        && orderid_passive_sell == 0 && orderid_init_sell_hedge == 0 && orderid_passive_sell_hedge == 0) {
        TradeData _trade = trade_queue_init_buy.front();
        double profit_cut = price_init_buy.value() - _trade.price;
        double price_sp_supplement = _trade.price - price_passive_buy.value();
        optional<double> max_sold = get_max_sold();
        optional<double> min_bought = get_min_bought();
        double profit_supplement;
        if (max_sold != nullopt) profit_supplement = max_sold.value() - price_sp_supplement;
        else if (min_bought != nullopt) profit_supplement = min_bought.value() - price_sp_supplement;
        else profit_supplement = 0;
        _ctx->stra_log_info(fmt::format("init_buy hedging Profit: cut:{}, supplement:{}",
                                        profit_cut, profit_supplement).c_str());

        if (profit_cut > profit_supplement) {
            double exceed_price = price_init_buy.value() - double(exceed_tick) * pricetick_init;
            orderid_init_sell_hedge = _ctx->stra_sell(wt_symbol_init.c_str(), exceed_price, _trade.qty, 1);
            _ctx->stra_log_info(fmt::format("hedge init_buy cut:{} price:{},volume:{}",
                                            orderid_init_sell_hedge, exceed_price, _trade.qty).c_str());
        } else {
            double exceed_price = price_passive_buy.value() - double(exceed_tick) * pricetick_passive;
            orderid_passive_sell_hedge = _ctx->stra_sell(wt_symbol_passive.c_str(), exceed_price, _trade.qty, 1);
            _ctx->stra_log_info(fmt::format("hedge init_buy supplement:{} price:{},volume:{}",
                                            orderid_passive_sell_hedge, exceed_price, _trade.qty).c_str());
        }
    }

    if (!trade_queue_passive_sell.empty() && trade_queue_passive_buy.empty() && trade_queue_init_buy.empty()
        && orderid_init_buy == 0 && orderid_init_buy_hedge == 0 && orderid_passive_buy_hedge == 0) {
        TradeData _trade = trade_queue_passive_sell.front();
        double profit_cut = _trade.price - price_passive_sell.value();
        double price_sp_supplement = price_init_sell.value() - _trade.price;
        optional<double> max_sold = get_max_sold();
        optional<double> min_bought = get_min_bought();
        double profit_supplement;
        if (max_sold != nullopt) profit_supplement = max_sold.value() - price_sp_supplement;
        else if (min_bought != nullopt) profit_supplement = min_bought.value() - price_sp_supplement;
        else profit_supplement = 0;
        _ctx->stra_log_info(
                fmt::format("passive_sell hedging Profit: cut:{}, supplement:{}", profit_cut,
                            profit_supplement).c_str());

        if (profit_cut > profit_supplement) {
            double exceed_price = price_passive_sell.value() + double(exceed_tick) * pricetick_passive;
            orderid_passive_buy_hedge = _ctx->stra_buy(wt_symbol_passive.c_str(), exceed_price, _trade.qty, 1);
            _ctx->stra_log_info(fmt::format("hedge passive_sell cut:{} price:{},volume:{}",
                                            orderid_passive_buy_hedge, exceed_price, _trade.qty).c_str());
        } else {
            double exceed_price = price_init_sell.value() + double(exceed_tick) * pricetick_init;
            orderid_init_buy_hedge = _ctx->stra_buy(wt_symbol_init.c_str(), exceed_price, _trade.qty, 1);
            _ctx->stra_log_info(fmt::format("hedge passive_sell supplement:{} price:{},volume:{}",
                                            orderid_init_buy_hedge, exceed_price, _trade.qty).c_str());
        }
    }

    if (!trade_queue_init_sell.empty() && trade_queue_passive_sell.empty() && trade_queue_passive_buy.empty()
        && orderid_passive_buy == 0 && orderid_passive_buy_hedge == 0 && orderid_init_buy_hedge == 0) {
        TradeData _trade = trade_queue_init_sell.front();
        double profit_cut = _trade.price - price_init_sell.value();
        double price_sp_supplement = _trade.price - price_passive_sell.value();
        optional<double> max_sold = get_max_sold();
        optional<double> min_bought = get_min_bought();
        double profit_supplement;
        if (min_bought != nullopt) profit_supplement = price_sp_supplement - min_bought.value();
        else if (max_sold != nullopt) profit_supplement = price_sp_supplement - max_sold.value();
        else profit_supplement = 0;
        _ctx->stra_log_info(fmt::format("init_sell hedging Profit: cut:{}, supplement:{}",
                                        profit_cut, profit_supplement).c_str());
        if (profit_cut > profit_supplement) {
            double exceed_price = price_init_sell.value() + double(exceed_tick) * pricetick_init;
            orderid_init_buy_hedge = _ctx->stra_buy(wt_symbol_init.c_str(), exceed_price, _trade.qty, 1);
            _ctx->stra_log_info(fmt::format("hedge init_sell cut:{} price:{},volume:{}",
                                            orderid_init_buy_hedge, exceed_price, _trade.qty).c_str());
        } else {
            double exceed_price = price_passive_sell.value() + double(exceed_tick) * pricetick_passive;
            orderid_passive_buy_hedge = _ctx->stra_buy(wt_symbol_passive.c_str(), exceed_price, _trade.qty, 1);
            _ctx->stra_log_info(fmt::format("hedge init_sell supplement:{} price:{},volume:{}",
                                            orderid_passive_buy_hedge, exceed_price, _trade.qty).c_str());
        }
    }

    if (!trade_queue_passive_buy.empty() && trade_queue_passive_sell.empty() && trade_queue_init_sell.empty()
        && orderid_init_sell == 0 && orderid_init_sell_hedge == 0 && orderid_passive_sell_hedge == 0) {
        TradeData _trade = trade_queue_passive_buy.front();
        double profit_cut = price_passive_buy.value() - _trade.price;
        double price_sp_supplement = price_init_buy.value() - _trade.price;
        optional<double> max_sold = get_max_sold();
        optional<double> min_bought = get_min_bought();
        double profit_supplement;
        if (min_bought != nullopt) profit_supplement = price_sp_supplement - min_bought.value();
        else if (max_sold != nullopt) profit_supplement = price_sp_supplement - max_sold.value();
        else profit_supplement = 0;
        _ctx->stra_log_info(fmt::format("passive_buy hedging Profit: cut:{}, supplement:{}",
                                        profit_cut, profit_supplement).c_str());

        if (profit_cut > profit_supplement) {
            double exceed_price = price_passive_buy.value() - double(exceed_tick) * pricetick_passive;
            orderid_passive_sell_hedge = _ctx->stra_sell(wt_symbol_passive.c_str(), exceed_price, _trade.qty, 1);
            _ctx->stra_log_info(fmt::format("hedge passive_buy: cut:{} price:{},volume:{}",
                                            orderid_passive_sell_hedge, exceed_price, _trade.qty).c_str());
        } else {
            double exceed_price = price_init_buy.value() - double(exceed_tick) * pricetick_init;
            orderid_init_sell_hedge = _ctx->stra_sell(wt_symbol_init.c_str(), exceed_price, _trade.qty, 1);
            _ctx->stra_log_info(fmt::format("hedge passive_buy: supplement:{} price:{},volume:{}",
                                            orderid_init_sell_hedge, exceed_price, _trade.qty).c_str());
        }
    }
}


void WtUftStraPair::on_position(IUftStraCtx *ctx, const char *stdCode, bool isLong, double prevol, double preavail,
                                double newvol, double newavail) {
    if (stdCode != wt_symbol_init && stdCode != wt_symbol_passive) {
        // _ctx->stra_log_error(fmt::format("on_position wrong symbol: {}", stdCode).c_str());
        return;
    }
    if (stdCode == wt_symbol_init) {
        if (isLong) position_init.pos_long = int(newvol + prevol);
        else position_init.pos_short = int(newvol + prevol);
        position_init.pos = position_init.pos_long - position_init.pos_short;
    } else {
        if (isLong) position_passive.pos_long = int(newvol + prevol);
        else position_passive.pos_short = int(newvol + prevol);
        position_passive.pos = position_passive.pos_long - position_passive.pos_short;
    }
    position_sp.pos_long = min(position_init.pos_long, position_passive.pos_short);
    position_sp.pos_short = min(position_init.pos_short, position_passive.pos_long);
    position_sp.pos = position_sp.pos_long - position_sp.pos_short;
    position_inited = true;
    print_position();
}


void WtUftStraPair::on_order(IUftStraCtx *ctx, uint32_t localid, const char *stdCode, bool isLong, uint32_t offset,
                             double totalQty, double leftQty, double price, bool isCanceled) {
    // _ctx->stra_log_info(fmt::format("on_order id[{}] name[{}]", ctx->id(), ctx->name()).c_str());
    // 过滤非本策略回报
    if (stdCode != wt_symbol_init && stdCode != wt_symbol_passive) {
        // _ctx->stra_log_error(fmt::format("other symbol: {}", code).c_str());
        return;
    }
    auto _totalQty = uint32_t(totalQty);
    auto _leftQty = uint32_t(leftQty);
    if (auto search = orders.find(localid); search == orders.end()) {
        // 下单成功时会新增订单
        ctx->stra_log_info(fmt::format("on_order new {} price[{}] isLong[{}] leftQty[{}]",
                                       localid, price, isLong, leftQty).c_str());
        orders[localid] = OrderData{stdCode, price, isLong, offset, _totalQty, _leftQty, isCanceled};
    } else {
        // 更新订单信息，用以判断订单是否处理完毕
        ctx->stra_log_info(fmt::format("on_order update {} price[{}] isLong[{}] leftQty[{}] isCanceled[{}]",
                                       localid, price, isLong, leftQty, isCanceled).c_str());
        orders[localid].leftQty = _leftQty;
        orders[localid].isCanceled = isCanceled;
    }
    // 判断订单是否结束
    refresh_orders_status();
}

void WtUftStraPair::print_position() {
    _ctx->stra_log_info(fmt::format("position {} {} init: {} {} passive: {} {}",
                                    position_sp.pos_long, position_sp.pos_short,
                                    position_init.pos_long, position_init.pos_short,
                                    position_passive.pos_long, position_passive.pos_short).c_str());
}

void WtUftStraPair::on_channel_ready(IUftStraCtx *ctx) {
    ctx->stra_log_info(fmt::format("on_channel_ready id[{}] name[{}]", ctx->id(), ctx->name()).c_str());
    //初始化仓位
    position_init.name = wt_symbol_init;
    position_passive.name = wt_symbol_passive;
    position_sp.name = strategy_name;
    if (!position_inited) {
        _ctx->stra_log_info(fmt::format("init position:{},{},{}",
                                        strategy_name, wt_symbol_init, wt_symbol_passive).c_str());
        _ctx->stra_enum_position(wt_symbol_init.c_str());
        _ctx->stra_enum_position(wt_symbol_passive.c_str());
    }

    // 订阅行情
    _ctx->stra_sub_ticks(wt_symbol_init.c_str());
    _ctx->stra_sub_ticks(wt_symbol_passive.c_str());
    _ctx->stra_sub_order_queues(wt_symbol_init.c_str());
    _ctx->stra_sub_order_queues(wt_symbol_passive.c_str());
    _ctx->stra_sub_order_details(wt_symbol_init.c_str());
    _ctx->stra_sub_order_details(wt_symbol_passive.c_str());
    _ctx->stra_sub_transactions(wt_symbol_init.c_str());
    _ctx->stra_sub_transactions(wt_symbol_passive.c_str());
    _ctx->stra_log_info("Subscribe market successfully.");

    _channel_ready = true;
}

void WtUftStraPair::on_channel_lost(IUftStraCtx *ctx) {
    _ctx->stra_log_info("on_channel_lost ......");
    _channel_ready = false;
    dump_trade_book();
    dump_position();
    dump_trade_all();
    dump_variable();
}

// 刷新当前订单状态
void WtUftStraPair::refresh_orders_status() {
    if (check_order_all_processed(orderid_init_buy)) {
        orderid_init_buy = 0;
        price_init_buy_order = 0;
    }
    if (check_order_all_processed(orderid_init_sell)) {
        orderid_init_sell = 0;
        price_init_sell_order = 0;
    }
    if (check_order_all_processed(orderid_passive_buy)) {
        orderid_passive_buy = 0;
        price_passive_buy_order = 0;
    }
    if (check_order_all_processed(orderid_passive_sell)) {
        orderid_passive_sell = 0;
        price_passive_sell_order = 0;
    }
    // hedge orderid
    if (check_order_all_processed(orderid_init_buy_hedge)) orderid_init_buy_hedge = 0;
    if (check_order_all_processed(orderid_init_sell_hedge)) orderid_init_sell_hedge = 0;
    if (check_order_all_processed(orderid_passive_buy_hedge)) orderid_passive_buy_hedge = 0;
    if (check_order_all_processed(orderid_passive_sell_hedge)) orderid_passive_sell_hedge = 0;
}

// 判断订单是否处理完毕的标准：要么撤单，要么成交且on_trade处理完毕
bool WtUftStraPair::check_order_all_processed(uint32_t localid) {
    if (auto search = orders.find(localid); search != orders.end()) {
        const OrderData &_order = search->second;
        if (_order.isCanceled) return true;
        else if (_order.leftQty == 0) {
            // 报单有成交时需确保on_trade处理完毕
            auto iter_trades = trades.find(localid);
            if (iter_trades != trades.end() && _order.totalQty == iter_trades->second) return true;
        }
    }
    return false;
}


void WtUftStraPair::dump_trade_book() {
    _ctx->stra_log_info(fmt::format("dump_trade_book trade_books.size={}", trade_books.size()).c_str());
    try {
        db.exec("DELETE FROM tradebook");
        for (const auto &[_, b]: trade_books) {
            if (b.buy == 0 && b.sell == 0 && b.bought == 0 && b.sold == 0) continue;
            SQLite::Statement insert(db, "insert into tradebook(name,price,buy,sell,bought,sold) "
                                         "VALUES (?, ?,?, ?,?, ?)");
            insert.bind(1, b.name);
            insert.bind(2, b.price);
            insert.bind(3, b.buy);
            insert.bind(4, b.sell);
            insert.bind(5, b.bought);
            insert.bind(6, b.sold);
            insert.exec();
        }
    }
    catch (std::exception &e) {
        _ctx->stra_log_error(fmt::format("SQLite exception: {}", e.what()).c_str());
    }
}

void WtUftStraPair::dump_trade_all() {
    _ctx->stra_log_info("dump_trade_all ...");
    try {
        for (auto it = trade_all.begin(); it != trade_all.end();) {
            SQLite::Statement insert(db, "insert into trade(name,date, time, price, direction, offset, volume) "
                                         "VALUES (?, ?, ?, ?, ?, ?, ?)");
            insert.bind(1, it->stdCode);
            insert.bind(2, trade_date);
            insert.bind(3, it->time);
            insert.bind(4, it->price);
            insert.bind(5, it->isLong ? "Buy " : "Sell");
            insert.bind(6, decodeOffset(it->offset));
            insert.bind(7, it->qty);
            insert.exec();
            it = trade_all.erase(it);
        }
    }
    catch (std::exception &e) {
        _ctx->stra_log_error(fmt::format("SQLite exception: {}", e.what()).c_str());
    }
}

void WtUftStraPair::dump_position() {
    _ctx->stra_log_info("dump_position ...");
    try {
        db.exec("DELETE FROM position");
        SQLite::Statement insert(db, "insert into position(name,pos,pos_long,pos_short) VALUES (?, ? , ?, ?)");

        insert.bind(1, position_init.name);
        insert.bind(2, position_init.pos);
        insert.bind(3, position_init.pos_long);
        insert.bind(4, position_init.pos_short);
        insert.exec();
        insert.reset();
        insert.bind(1, position_passive.name);
        insert.bind(2, position_passive.pos);
        insert.bind(3, position_passive.pos_long);
        insert.bind(4, position_passive.pos_short);
        insert.exec();
        insert.reset();
        insert.bind(1, position_sp.name);
        insert.bind(2, position_sp.pos);
        insert.bind(3, position_sp.pos_long);
        insert.bind(4, position_sp.pos_short);
        insert.exec();
    }
    catch (std::exception &e) {
        _ctx->stra_log_error(fmt::format("SQLite exception: {}", e.what()).c_str());
    }
}

void WtUftStraPair::dump_variable() {
    _ctx->stra_log_info("dump_variable ...");
    save_variable(db, "trade_date", trade_date);
    save_variable(db, "cancel_count_init", to_string(cancel_count_init));
    save_variable(db, "cancel_count_passive", to_string(cancel_count_passive));
}

void WtUftStraPair::on_exit(IUftStraCtx *ctx) {
    ctx->stra_log_info("on_exit cancel orders and dump data ...");
    isStarted = false;
    ctx->stra_cancel_all(wt_symbol_init.c_str());
    ctx->stra_cancel_all(wt_symbol_passive.c_str());
    dump_trade_book();
    dump_position();
    dump_trade_all();
    dump_variable();
}

//更新仓位
void WtUftStraPair::update_position() {
    _ctx->stra_log_info(fmt::format("update_position:{},{}", wt_symbol_init, wt_symbol_passive).c_str());
    double _pos_init = _ctx->stra_enum_position(wt_symbol_init.c_str());
    double _pos_passive = _ctx->stra_enum_position(wt_symbol_passive.c_str());
    if (decimal::eq(_pos_init, 0) && decimal::eq(_pos_passive, 0)) {
        _ctx->stra_log_info(fmt::format("{},{} position empty，position init end.",
                                        wt_symbol_init, wt_symbol_passive).c_str());
        position_inited = true;
    }
}